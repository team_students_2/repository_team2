package travel.agency;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

interface Refreshable {
    void refresh();
}

public class Panel_util implements ActionListener, ComponentListener {

    JPanel cards; //a panel that uses CardLayout
    final static String ADDUSER = "Add User";
    final static String ADDOFFICE = "Add Office";
    final static String ADDDIRECTION = "Add Direction";
    final static String ADDHOTEL = "Add Hotel";
    
    
    
    final static String NOTHING1 = "Empty1";

    public JMenuBar createMenuBar() {
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;
        JMenuItem menuItem2;
        JMenuItem menuItem3;
        JMenuItem menuItem4;

        //Create the menu bar.
        menuBar = new JMenuBar();

        menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);

        menuItem = new JMenuItem(ADDOFFICE, KeyEvent.VK_T);
        menuItem.setMnemonic(KeyEvent.VK_T); //used constructor instead
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem2 = new JMenuItem(ADDDIRECTION, KeyEvent.VK_Y);
        menuItem2.setMnemonic(KeyEvent.VK_Y); //used constructor instead
        menuItem2.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_2, ActionEvent.ALT_MASK));
        menuItem2.addActionListener(this);
        menu.add(menuItem2);
        
        menuItem3 = new JMenuItem(ADDUSER, KeyEvent.VK_Z);
        menuItem3.setMnemonic(KeyEvent.VK_Z); //used constructor instead
        menuItem3.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_3, ActionEvent.ALT_MASK));
        menuItem3.addActionListener(this);
        menu.add(menuItem3);
        
        menuItem4 = new JMenuItem(ADDHOTEL, KeyEvent.VK_Z);
        menuItem4.setMnemonic(KeyEvent.VK_Z); //used constructor instead
        menuItem4.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_3, ActionEvent.ALT_MASK));
        menuItem4.addActionListener(this);
        menu.add(menuItem4);
        
        return menuBar;
    }

    public Container createContentPane() {
        //Create the content-pane-to-be.

        Card0 card0 = new Card0();
        Office_form card1 = new Office_form();
        Direction_form card2 = new Direction_form();
        AddUser_form card3 = new AddUser_form();
        Hotel_form card4 = new Hotel_form();
     
        card4.addComponentListener(this);
        
        card1.init();
        card2.init();
        card3.init();
        card4.init();
        
        
         //Create the panel that contains the "cards".
        cards = new JPanel(new CardLayout());
        cards.add(card0, NOTHING1);
        cards.add(card1, ADDOFFICE);
        cards.add(card2, ADDDIRECTION);
        cards.add(card3, ADDUSER);
        cards.add(card4, ADDHOTEL);

        JPanel p1 = new JPanel();

        p1.add(cards, BorderLayout.CENTER);
        return p1;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JMenuItem source = (JMenuItem) (e.getSource());
        CardLayout cl = (CardLayout) (cards.getLayout());
        
       switch (source.getText()) {
            case ADDOFFICE:
                cl.show(cards, ADDOFFICE);
                break;   
            case ADDDIRECTION:
                cl.show(cards, ADDDIRECTION);
                break;
            case ADDUSER:
                cl.show(cards, ADDUSER);
                break;
            case ADDHOTEL:
 
               //Hotel_form helper = (Hotel_form) cards.getComponent(3);
                //helper.refresh();
                cl.show(cards, ADDHOTEL);
    
                
                
        }
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Travel Agency");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(800, 460);
        //Create and set up the content pane.
        Panel_util demo = new Panel_util();
        frame.setJMenuBar(demo.createMenuBar());
        frame.setContentPane(demo.createContentPane());

        //Display the window.
        frame.setVisible(true);
    }
    
    @Override
    public void componentShown(ComponentEvent e) {
       if(e.getComponent().getClass() == Hotel_form.class)
       {
           Hotel_form helper = (Hotel_form) e.getComponent();
           helper.refresh();
       }
    }
    @Override
    public void componentHidden(ComponentEvent e) {
       // displayMessage(e.getComponent().getClass().getName() + " --- Hidden");
    }
    @Override
    public void componentMoved(ComponentEvent e) {
       // displayMessage(e.getComponent().getClass().getName() + " --- Moved");
    }
    @Override
    public void componentResized(ComponentEvent e) {
       // displayMessage(e.getComponent().getClass().getName() + " --- Resized ");            
    }

    
 }

