/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business_tier_ejb;

import java.util.List;
import javax.ejb.Stateless;
import sub_buisness_tier.TFacade;

/**
 *
 * @author PWR
 */
@Stateless
public class Facade_ejb implements Facade_ejbRemote {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    TFacade facade = new TFacade();
    
    
    public String add_user(String data[]) {
       return facade.add_user(data);
    }


//OFFICE HANDLING

    public String addOffice(String data[]) {
       return facade.addOffice(data);
    }

   


//DIRECTION HANDLING
    public String addDirection(String data[]) {
        return facade.addDirection(data);
    }

  
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT ADDING
    public String addAirport(String data[], String directiondata[]){
      return facade.addAirport(data, directiondata);
    }


//ALIMENTATION ADDING
    public String addAlimentation(String data[], String directiondata[], String hoteldata[]) {
       return facade.addAlimentation(data, directiondata, hoteldata);
    }


//HOTEL ADDING
    public String addHotel(String data[], String directiondata[]){
       return facade.addHotel(data, directiondata);
    }

//SPORT AND ENTERTAINMENT ADDING
    public String addSportEntertainment(String data[], String directiondata[], String hoteldata[]){
       return facade.addSportEntertainment(data, directiondata, hoteldata);
    }

    public List<String[]> getDirectionDataList() {
      return facade.getDirectionDataList();
    }

}
