package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class AddUser_form extends JPanel implements ActionListener {

    JLabel lusername = new JLabel("Username");
    JTextField username = new JTextField(30);
    JLabel lpassword = new JLabel("Password");
    JPasswordField password = new JPasswordField(30);
    JButton add_user = new JButton("Sign Up");

    AddUser_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lusername);
        add(username);
        add(lpassword);
        add(password);
        add(add_user);
    }

    public void init() {
        add_user.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String[] data = form_adduser();
        if (data == null) {
            return;
        }
        Client.getFacade().add_user(data);                                      
    }

    public String[] form_adduser() {
        if (content_validate(username) == null) {
            return null;
        }
        if (password_securitycheck(password) == null) {
            return null;
        }
        String cPasswd = new String(password.getPassword());
        String data[] = {(String) username.getText(),
            cPasswd};
        return data;
    }
    
    public String content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
   
    
    public String password_securitycheck(JPasswordField val){
    String cPasswd = new String(val.getPassword());
    if (cPasswd == null){
        JOptionPane.showMessageDialog(this, "Password cannot be empty");
        return null;
    }
    if (cPasswd.length() < 6 || cPasswd.length() > 20){
        JOptionPane.showMessageDialog(this, "Password must be between 6 and 20 characters");
        return null;
    }
    boolean containsUpperCase = false;
    boolean containsLowerCase = false;
    boolean containsDigit = false;
    for(char ch: cPasswd.toCharArray()){
        if(Character.isUpperCase(ch)) containsUpperCase = true;
        if(Character.isLowerCase(ch)) containsLowerCase = true;
        if(Character.isDigit(ch)) containsDigit = true;
    }
    if(containsUpperCase && containsLowerCase && containsDigit){
        return cPasswd;
    }
    JOptionPane.showMessageDialog(this, "Password incorrect, it must contain at least:\n1 uppercase character\n1 lowercase character\n1 digit");
    return null;
    }
    
}


