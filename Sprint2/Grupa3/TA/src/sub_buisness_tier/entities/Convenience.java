package sub_buisness_tier.entities;

import java.util.Objects;

/**
 * Created by jm on 26.04.17.
 */
public class Convenience {
    protected String type;
    protected int price;

//GETTERS AND SETTERS
    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Convenience other = (Convenience) obj;
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
