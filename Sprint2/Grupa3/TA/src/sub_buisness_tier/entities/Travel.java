package sub_buisness_tier.entities;

public class Travel {
    
    private Office m_office;
    private TUser m_user;
    private Period m_period;
    private Direction m_direction;
    private Hotel m_hotel;
    private AirPort m_airport;
    private Alimentation alimentation;
    private SportEntertainment sport;
    private String typ_wyjazduData;

    public Travel(Office m_office, TUser m_user, Period m_period, Direction m_direction, Hotel m_hotel, AirPort m_airport, Alimentation alimentation, SportEntertainment sport, String typ_wyjazduData) {
        this.m_office = m_office;
        this.m_user = m_user;
        this.m_period = m_period;
        this.m_direction = m_direction;
        this.m_hotel = m_hotel;
        this.m_airport = m_airport;
        this.alimentation = alimentation;
        this.sport = sport;
        this.typ_wyjazduData = typ_wyjazduData;
    }

    public Office getM_office() {
        return m_office;
    }

    public TUser getM_user() {
        return m_user;
    }

    public Period getM_period() {
        return m_period;
    }

    public Direction getM_direction() {
        return m_direction;
    }

    public Hotel getM_hotel() {
        return m_hotel;
    }

    public AirPort getM_airport() {
        return m_airport;
    }

    public Alimentation getAlimentation() {
        return alimentation;
    }

    public SportEntertainment getSport() {
        return sport;
    }
    
    public String getType() {
        return typ_wyjazduData;
    }
    
    public String[] getDane()
    {
        String data[] = new String [6];
                data[0]= getM_airport().getCity();
                data[1]= getM_hotel().getName();
                data[2]= getM_hotel().getStandard();
                data[3]= getAlimentation().meals;
                data[4]= getSport().type;
                data[5]= getM_period().getM_days() + " Dni";
                //data[6]= this.typ_wyjazduData;
                return data;
    }
}
