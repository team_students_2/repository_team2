/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier;

import sub_buisness_tier.entities.AirPort;
import sub_buisness_tier.entities.Alimentation;
import sub_buisness_tier.entities.Direction;
import sub_buisness_tier.entities.Hotel;
import sub_buisness_tier.entities.Office;
import sub_buisness_tier.entities.SportEntertainment;
import sub_buisness_tier.entities.TUser;

/**
 *
 * @author Marcin
 */
public class Dane {
    
    String dane_user1[] = { "Username1","Password1" };
    String dane_user2[] = { "Username2","Password2" };
    String dane_user3[] = { "Username3","Password3" };
    String dane_direction1[] = {"Country1"};
    String dane_direction2[] = {"Country2"};
    String dane_direction3[] = {"Country3"};
    String dane_hotel1[] = { "Hotel1", "Street1","Postal1","City1","Standard1" };
    String dane_hotel2[] = { "Hotel2", "Street2","Postal2","City1","Standard2" };
    String dane_hotel3[] = { "Hotel3", "Street3","Postal3","City2","Standard2" };
    String dane_office1[] = {"Street1","Mail1"};
    String dane_office2[] = {"Street2","Mail2"};
    String dane_office3[] = {"Street3","Mail3"};
    String dane_airport1[] = {"Country1","City1","Code1"};
    String dane_airport2[] = {"Country2","City2","Code2"};
    String dane_airport3[] = {"Country2","City3","Code3"};
    String dane_alimentation1[] = {"Meals1","10"};
    String dane_alimentation2[] = {"Meals2","20"};
    String dane_alimentation3[] = {"Meals3","30"};
    String dane_sport1[] = {"Type1","10"};
    String dane_sport2[] = {"Type2","20"};
    String dane_sport3[] = {"Type3","30"};
    TFacade fasada = new TFacade();
    
    public Direction kierunek(String dane_kierunku[])
    {
        Direction kier = new Direction();
        kier.setCountry(dane_kierunku[0]);
        return kier;
    }
    
    public TUser klient(String dane_klienta[])
    {
        TUser user = null;
        user = new TUser();

        user.setUsername(dane_klienta[0]);
        user.setPassword(dane_klienta[1]);

        return user;
    }
    
    public Office biuro(String data[]) {
        Office office = null;
        office = new Office();
        office.setCity(data[0]);
        office.setMail(data[1]);

        return office;
    }
    
    public AirPort lotnisko(String data[]) {
        AirPort airport = null;

        airport = new AirPort();
        airport.setCountry(data[0]);
        airport.setCity(data[1]);
        airport.setCode(data[2]);

        return airport;
    }
    
    public Alimentation udogodnienie(String data[]) {
        Alimentation alimentation = null;
        alimentation = new Alimentation();
        alimentation.setMeals(data[0]);
        alimentation.setPrice(Integer.parseInt(data[1]));

        return alimentation;
    }
    
    public Hotel hotel(String data[]) {
        Hotel hotel = null;
        hotel = new Hotel();
        hotel.setCity(data[0]);
        hotel.setName(data[1]);
        hotel.setPostalCode(data[2]);
        hotel.setStandard(data[3]);
        hotel.setStreet(data[4]);

        return hotel;
    }
    
    public SportEntertainment sport(String data[]) {
        SportEntertainment sport = null;
        sport = new SportEntertainment();
        sport.setType(data[0]);
        sport.setPrice(Integer.parseInt(data[1]));

        return sport;
    }
}
