/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier_ejb;

import javax.ejb.Stateless;
import sub_buisness_tier.TFacade;

/**
 *
 * @author Damian
 */
@Stateless
public class Facade_sub_buisness_tier_ejb implements Facade_sub_buisness_tier_ejbRemote {

    TFacade facade = new TFacade();
    
    public void clientOffer(String userData[], String directionData[], String hotelData[], String alimentationData[], String sportData[], String typ_wyjazduData[], String periodData[], String officeData[]) {
        facade.clientOffer(userData, directionData, hotelData, alimentationData, sportData, typ_wyjazduData, periodData, officeData);
    }
    
    public Object[][] getOffice() {
        return facade.getOffice();
    }

    public Object[][] getDirection() {
        return facade.getDirection();
    }

    public Object[][] getHotel(String directionData[]) {
        return facade.getHotel(directionData);
    }
    
    public Object[][] getAlimentation(String directionData[], String hotelData[]) {
        return facade.getAlimentation(directionData, hotelData);
    }
    
    public Object[][] getSport(String directionData[], String hotelData[]) {
        return facade.getSport(directionData, hotelData);
    }
}
