/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier_ejb;

import javax.ejb.Remote;

/**
 *
 * @author Damian
 */
@Remote
public interface Facade_sub_buisness_tier_ejbRemote {
    
    public void clientOffer(String userData[], String directionData[], String hotelData[], String alimentationData[], String sportData[], String typ_wyjazduData[], String periodData[], String officeData[]);
    public Object[][] getOffice();
    public Object[][] getDirection();
    public Object[][] getHotel(String directionData[]);
    public Object[][] getAlimentation(String directionData[], String hotelData[]);
    public Object[][] getSport(String directionData[], String hotelData[]);
}
