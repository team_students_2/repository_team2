package client_tier;

import sub_buisness_tier.TFacade;
import travel.agency.Panel_util;

/**
 *
 * @author Damian
 */
public class Client {
    static TFacade facade = new TFacade();

    static public TFacade getFacade() {
        return facade; }

    static public void setFacade(TFacade facade) {
        Client.facade = facade; }
    
    public static void main(String[] args) {
       java.awt.EventQueue.invokeLater(() -> {
           Panel_util.createAndShowGUI();
       });
    } 
}
