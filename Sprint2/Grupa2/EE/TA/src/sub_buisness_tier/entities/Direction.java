package sub_buisness_tier.entities;

import java.util.ArrayList;
import java.util.List;
import sub_buisness_tier.TFactory;

public class Direction {
    protected String Country;
    protected List<Hotel> hotelList;
    private AirPort airPort;


    public Direction() {
        hotelList = new ArrayList<>();
    }
/////////////////////////////////////////////////////////////////////////////////////////////

public Object[] checkAirport(String data[])
{
    TFactory factory = new TFactory();
    Period period = factory.createPeriod(data);
    if(airPort.checkFlight(period) == true) {
        System.out.println("OK7");
        return new Object[] {airPort, period};
    }      
    
    return null;
}

public Object[] checkHotel(String hotelData[], String alimentationData[], String sportData[], String periodData[])
{
    //przegladamy wszystkie hotele i jezeli znajdziemy odpowiedni to go zwracamy
    for(Hotel h : hotelList)
    {
        Object alimentationSport[] = h.checkStandards(hotelData, alimentationData, sportData);
        if(alimentationSport != null) {
            System.out.println("OK8");
            if(h.checkAvailability(periodData) == true) {
                System.out.println("OK6");
                return new Object[] {h, alimentationSport[0], alimentationSport[1]};
            }
        }
            
    }
    
    //jezeli nie znalezlismy odpowiedniego hotelu to zwracamy null
    return null;
}

//HOTEL HANDLING
    public void addHotel(String Data[]){
        TFactory factory = new TFactory();
        Hotel hotel = factory.createHotel(Data);
        if ((searchHotel(hotel)) == null) {
            hotelList.add(hotel);
            System.out.println("Dodano hotel" + hotel);


        }
        else
            System.out.println(hotel + "jest juz dodany");
    }

    public Hotel searchHotel(Hotel hotel) {
        int idx = hotelList.indexOf(hotel);
        if (idx != -1)
            return hotelList.get(idx);
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

//ALIMENTATION HANDLING
    public void addAlimentation(String data[], Hotel hotel){
        hotel = searchHotel(hotel);
        if (hotel != null){
            hotel.addAlimentation(data);
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT HANDLING
    public void addAirport (String Data[]){
        TFactory factory = new TFactory();
        AirPort airPort = factory.create_airport(Data);
        setAirPort(airPort);
        System.out.println("Przypisano lotnisko" + airPort);
    };



///////////////////////////////////////////////////////////////////////////////////////////

//SPORT AND ENTERTAINMENT HANDLING
    public void addSportEntertainment(String data[], Hotel hotel){
        hotel = searchHotel(hotel);
        if (hotel != null){
            hotel.addSportEntertainment(data);
        }

    }
//GETTERS AND SETTERS
    public String getCountry() {
        return Country;
    }
    public void setCountry(String country) {
        Country = country;
    }

    public AirPort getAirPort() {
        return airPort;
    }

    public void setAirPort(AirPort airPort) {
        this.airPort = airPort;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "Country='" + Country + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return Country == ((Direction) obj).getCountry();
    }

    public String[] toString_() {
        return new String[] {getCountry()};
    }
    
    public Object[][] getHotel() {
        Object[][] holel = new Object[hotelList.size()][];
        int i = 0;
        for (Hotel next : hotelList) {
            holel[i++] = next.toString_();
        }
        return holel;
    }
    
    public Object[][] getAlimentation(String hotelData[]) {
        TFactory factory = new TFactory();
        Hotel hotel = factory.createHotel(hotelData);
        hotel = searchHotel(hotel);
        if (hotel != null) {
            return hotel.getAlimentation();
        }
        return null;
    }
    
    public Object[][] getSport(String hotelData[]) {
        TFactory factory = new TFactory();
        Hotel hotel = factory.createHotel(hotelData);
        hotel = searchHotel(hotel);
        if (hotel != null) {
            return hotel.getSport();
        }
        return null;
    }
}