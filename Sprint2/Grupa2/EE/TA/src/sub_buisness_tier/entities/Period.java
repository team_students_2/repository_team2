package sub_buisness_tier.entities;

import java.util.Date;
import java.util.Objects;

public class Period {

    private String m_days;
    private String m_start;
    private String m_end;

    public String getM_days() {
        return m_days;
    }

    public void setM_days(String m_days) {
        this.m_days = m_days;
    }

    public String getM_start() {
        return m_start;
    }

    public void setM_start(String m_start) {
        this.m_start = m_start;
    }

    public String getM_end() {
        return m_end;
    }

    public void setM_end(String m_end) {
        this.m_end = m_end;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.m_days);
        hash = 97 * hash + Objects.hashCode(this.m_start);
        hash = 97 * hash + Objects.hashCode(this.m_end);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Period other = (Period) obj;
        if (!Objects.equals(this.m_days, other.m_days)) {
            return false;
        }
        if (!Objects.equals(this.m_start, other.m_start)) {
            return false;
        }
        if (!Objects.equals(this.m_end, other.m_end)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Period{" + "m_days=" + m_days + ", m_start=" + m_start + ", m_end=" + m_end + '}';
    }
}
