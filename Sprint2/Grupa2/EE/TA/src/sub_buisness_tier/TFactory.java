package sub_buisness_tier;

import sub_buisness_tier.entities.AirPort;
import sub_buisness_tier.entities.Alimentation;
import sub_buisness_tier.entities.Direction;
import sub_buisness_tier.entities.Hotel;
import sub_buisness_tier.entities.Office;
import sub_buisness_tier.entities.Period;
import sub_buisness_tier.entities.SportEntertainment;
import sub_buisness_tier.entities.TUser;

/**
 * Created by jm on 19.04.17.
 */
public class TFactory {

    public Office createOffice(String data[]) {
        Office office = null;
        office = new Office();
        office.setCity(data[0]);
        office.setMail(data[1]);

        return office;
    }

    public Alimentation createAlimentation(String data[]) {
        Alimentation alimentation = null;
        alimentation = new Alimentation();
        alimentation.setMeals(data[0]);
        alimentation.setPrice(Integer.parseInt(data[1]));

        return alimentation;
    }
    public Hotel createHotel(String data[]) {
        Hotel hotel = null;
        hotel = new Hotel();
        hotel.setCity(data[0]);
        hotel.setName(data[1]);
        hotel.setPostalCode(data[2]);
        hotel.setStandard(data[3]);
        hotel.setStreet(data[4]);

        return hotel;
    }

    public Direction createDirection(String data[]) {
        Direction direction = null;
        direction = new Direction();
        direction.setCountry(data[0]);

        return direction;
    }

    public SportEntertainment createSportEntertainment(String data[]) {
        SportEntertainment sport = null;
        sport = new SportEntertainment();
        sport.setType(data[0]);
        sport.setPrice(Integer.parseInt(data[1]));

        return sport;
    }

    public TUser create_user(String data[]){
        TUser user = null;
        user = new TUser();

        user.setUsername(data[0]);
        user.setPassword(data[0]);

        return user;
    }

    public AirPort create_airport(String data[]) {
        AirPort airport = null;

        airport = new AirPort();
        airport.setCountry(data[0]);
        airport.setCity(data[1]);
        airport.setCode(data[2]);

        return airport;
    }

    public Period createPeriod(String data[]) {
        Period period = null;
        
        period = new Period();
        period.setM_days(data[0]);
        period.setM_start(data[1]);
        period.setM_end(data[2]);
        
        return period;
    }
}