package sub_buisness_tier.entities;

public class TJourneyType {

    private String journeytype;
   
        public String getJourneyType() {
        return journeytype;
    }
 
    public void setJourneyType(String journeytype) {
        this.journeytype = journeytype;
    }
    
    @Override
    public String toString() {
            return getJourneyType();
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if(obj == null){
            return false;
        }
        if(!TJourneyType.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        final TJourneyType other = (TJourneyType) obj;
        if((this.journeytype == null) ? (other.journeytype != null) : !this.journeytype.equals(other.journeytype)){
            return false;
        }
        return true;
    }

}