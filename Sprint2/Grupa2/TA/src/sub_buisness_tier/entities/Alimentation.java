package sub_buisness_tier.entities;

public class Alimentation {
    protected String meals;
    protected int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getMeals() {
        return meals;
    }


    @Override
    public String toString() {
        return "Alimentation{" +
                "meals='" + meals + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return meals == ((Alimentation) obj).getMeals();
    }
    
    public String[] toString_() {
        return new String[] {getMeals(), Integer.toString(getPrice())};
    }
}