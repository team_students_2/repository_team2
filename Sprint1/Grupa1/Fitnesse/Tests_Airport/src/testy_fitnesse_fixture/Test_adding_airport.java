/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testy_fitnesse_fixture;

import fit.ColumnFixture;
import java.util.IllegalFormatCodePointException;
/**
 *
 * @author LukaszLech
 */
public class Test_adding_airport extends ColumnFixture {
    public String data[];
    
    public boolean add_airport() throws IllegalFormatCodePointException {
        int numberOfAirports = number_of_airports();
        
        try {
            SetUp.facade.add_airport(data);
            int numberOfAirportsAfterAddition = number_of_airports();
            return numberOfAirports != numberOfAirportsAfterAddition;
        } catch(IllegalFormatCodePointException e) {
            
        }
        return false;
    }
    
    public int number_of_airports() {
        return SetUp.facade.getmAirport().size();
    }
}
