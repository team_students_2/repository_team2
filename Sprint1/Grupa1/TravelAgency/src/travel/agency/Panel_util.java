package travel.agency;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 *
 * @author LukaszLech
 */
public class Panel_util implements ActionListener {

    JPanel cards; //a panel that uses CardLayout
    final static String AIRPORT = "Airport";
    final static String JOURNEYTYPE = "Journey Type";
    final static String ADDUSER = "Add User";
    final static String NOTHING1 = "Empty1";

    public JMenuBar createMenuBar() {
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;
        JMenuItem menuItem2;
        JMenuItem menuItem3;

        //Create the menu bar.
        menuBar = new JMenuBar();

        menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);

        menuItem = new JMenuItem(AIRPORT, KeyEvent.VK_T);
        menuItem.setMnemonic(KeyEvent.VK_T); //used constructor instead
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem2 = new JMenuItem(JOURNEYTYPE, KeyEvent.VK_Y);
        menuItem2.setMnemonic(KeyEvent.VK_Y); //used constructor instead
        menuItem2.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_2, ActionEvent.ALT_MASK));
        menuItem2.addActionListener(this);
        menu.add(menuItem2);
        
        menuItem3 = new JMenuItem(ADDUSER, KeyEvent.VK_Z);
        menuItem3.setMnemonic(KeyEvent.VK_Z); //used constructor instead
        menuItem3.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_3, ActionEvent.ALT_MASK));
        menuItem3.addActionListener(this);
        menu.add(menuItem3);
        
        return menuBar;
    }

    public Container createContentPane() {
        //Create the content-pane-to-be.

        Card0 card0 = new Card0();
        Airport_form card1 = new Airport_form();
        JourneyType_form card2 = new JourneyType_form();
        AddUser_form card3 = new AddUser_form();
        card1.init();
        card2.init();
        card3.init();

        //Create the panel that contains the "cards".
        cards = new JPanel(new CardLayout());
        cards.add(card0, NOTHING1);
        cards.add(card1, AIRPORT);
        cards.add(card2, JOURNEYTYPE);
        cards.add(card3, ADDUSER);

        JPanel p1 = new JPanel();

        p1.add(cards, BorderLayout.CENTER);
        return p1;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JMenuItem source = (JMenuItem) (e.getSource());
        CardLayout cl = (CardLayout) (cards.getLayout());
        switch (source.getText()) {
            case AIRPORT:
                cl.show(cards, AIRPORT);
                break;   
            case JOURNEYTYPE:
                cl.show(cards, JOURNEYTYPE);
                break;
            case ADDUSER:
                cl.show(cards, ADDUSER);
                break;
        }
    }

    /**
     * Create the GUI and show it. For thread safety, this method should be
     * invoked from the event-dispatching thread.
     */
    public static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Travel Agency");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(800, 460);
        //Create and set up the content pane.
        Panel_util demo = new Panel_util();
        frame.setJMenuBar(demo.createMenuBar());
        frame.setContentPane(demo.createContentPane());

        //Display the window.
        frame.setVisible(true);
    }

}

