package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author LukaszLech
 */
public class Airport_form extends JPanel implements ActionListener {

    JLabel lcountry = new JLabel("Country");
    JTextField country = new JTextField(30);
    JLabel lcity = new JLabel("City");
    JTextField city = new JTextField(30);
    JLabel lcode = new JLabel("Code");
    JTextField code = new JTextField(5);
    JButton add_airport = new JButton("Add airport");

    Airport_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lcountry);
        add(country);
        add(lcity);
        add(city);
        add(lcode);
        add(code);
        add(add_airport);
    }

    public void init() {
        add_airport.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String[] data = form_airport();
        if (data == null) {
            return;
        }
        Client.getFacade().add_airport(data);
    }

    public String[] form_airport() {
        if (content_validate(country) == null) {
            return null;
        }
        if (content_validate(city) == null) {
            return null;
        }
        if (content_validate(code) == null) {
            return null;
        }

        String data[] = {(String) country.getText(),
            (String) city.getText(), (String) code.getText()};
        return data;
    }

    public String content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
}


