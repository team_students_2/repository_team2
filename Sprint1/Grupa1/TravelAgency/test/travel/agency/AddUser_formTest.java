/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.agency;

import javax.swing.JPasswordField;
import javax.swing.JTextField;
import static org.junit.Assert.assertEquals;
import org.junit.experimental.theories.*;
import org.junit.runner.RunWith;

/**
 *
 * @author Wiktor
 */
@RunWith(Theories.class)
public class AddUser_formTest {
    
    public AddUser_formTest() {
    }


    /**
     * Test of content_validate method, of class AddUser_form.
     */
    @org.junit.Test
    public void testContent_validate() {
        System.out.println("content_validate");
        JTextField val = new JTextField("Testing Content Validate");
        AddUser_form instance = new AddUser_form();
        String expResult = "Testing_Content_Validate";
        String result = instance.content_validate(val);
        assertEquals(expResult, result);
    }

    /**
     * Test of password_securitycheck method, of class AddUser_form.
     */
    @Theory
    public void testPassword_securitycheck(String expResult, String password) {
        System.out.println("password_securitycheck");
        JPasswordField val = new JPasswordField(password);
        AddUser_form instance = new AddUser_form();
        String result = instance.password_securitycheck(val);
        assertEquals(result, expResult);
    }

    public static @DataPoints String[] expResult = {null,null,"Azazel123",null};
    public static @DataPoints String[] password = {null,"beelzebub","Azazel123","damballa1"};
            
    
}
