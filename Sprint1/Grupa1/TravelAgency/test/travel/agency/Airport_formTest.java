/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.agency;

import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Wiktor
 */
public class Airport_formTest {
    
    public Airport_formTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void testForm_airport() {
        System.out.println("form_airport");
        Airport_form instance = new Airport_form();
        instance.country.setText("CountryName");
        instance.city.setText("CityName");
        instance.code.setText("Code");
        String[] expResult = {"CountryName","CityName","Code"};
        String[] result = instance.form_airport();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of content_validate method, of class Airport_form.
     */
    @Test
    public void testContent_validate() {
        System.out.println("content_validate");
        JTextField val = new JTextField("Testing Content Validate");
        Airport_form instance = new Airport_form();
        String expResult = "Testing_Content_Validate";
        String result = instance.content_validate(val);
        assertEquals(expResult, result);
    }
    
}
