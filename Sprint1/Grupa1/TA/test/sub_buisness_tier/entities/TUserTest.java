/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier.entities;

import org.junit.Test;
import static org.junit.Assert.*;


public class TUserTest {
    
    public TUserTest() {
    }

    /**
     * Test of getUsername method, of class TUser.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        TUser instance = new TUser();
        instance.setUsername("123");
        String expResult = "123";
        String result = instance.getUsername();
        assertEquals(expResult, result);
     }

    /**
     * Test of getPassword method, of class TUser.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        TUser instance = new TUser();
        instance.setPassword("123");
        String expResult = "123";
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUsername method, of class TUser.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String username = "123";
        TUser instance = new TUser();
        instance.setUsername(username);
        assertEquals(username, instance.getUsername());
    }

    /**
     * Test of setPassword method, of class TUser.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "123";
        TUser instance = new TUser();
        instance.setPassword(password);
        assertEquals(password, instance.getPassword());
    }

    /**
     * Test of toString method, of class TUser.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TUser instance = new TUser();
        instance.setUsername("123");
        instance.setPassword("456");
        String expResult = "Username : " + "123" + ", Password: " + "456";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    
}
