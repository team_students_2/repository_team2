/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier;

import java.util.ArrayList;
import java.util.List;
import sub_buisness_tier.entities.TAirport;
import sub_buisness_tier.entities.TJourneyType;
import sub_buisness_tier.entities.TUser;
/**
 *
 * @author LukaszLech
 */
public class TFacade {
    
    List<TAirport> mAirport;
    List<TJourneyType> mJourneyType;
    List<TUser> mUser;
    
    public TFacade() {
        mAirport = new ArrayList<>();
        mJourneyType = new ArrayList<>();
        mUser = new ArrayList<>();
    }

    public void setmAirport(List<TAirport> mAirport) {
        this.mAirport = mAirport;
    }

    public List<TAirport> getmAirport() {
        return mAirport;
    }

    public String add_airport(String data[]) {
        TFactory factory = new TFactory();
        TAirport airport = factory.create_airport(data);
        
        if (search_airport(airport) == null) {
            mAirport.add(airport);
            System.out.println("Added");
            return airport.toString();
        }
        
        return null;
    }
    
        public TAirport search_airport (TAirport airport) {
        int idx;
        if ((idx = mAirport.indexOf(airport)) != -1) {
            airport = mAirport.get(idx);
            return airport;
        }
        return null;
    }
        
    public void setmJourneyType(List<TJourneyType> mJourneyType) {
        this.mJourneyType = mJourneyType;
    }
       
    public List<TJourneyType> getmJourneyType() {
        return mJourneyType;
    }
    
    public TJourneyType search_journeytype (TJourneyType journeytype) {
        int idx;
        if ((idx = mJourneyType.indexOf(journeytype)) != -1) {
            journeytype = mJourneyType.get(idx);
            return journeytype;
        }
        return null;
    }
            
    public String add_journeytype(String data) {
        TFactory factory = new TFactory();
        TJourneyType journeytype = factory.create_journeytype(data);
        
        if (search_journeytype(journeytype) == null) {
            mJourneyType.add(journeytype);
            System.out.println("Added");
            return journeytype.toString();
        }
        
        return null;
    }

    
    public void setmUser(List<TUser> mUser) {
        this.mUser = mUser;
    }

    public List<TUser> getmUser() {
        return mUser;
    }
    
    public TUser search_user (TUser user) {
        int idx;
        if ((idx = mUser.indexOf(user)) != -1) {
            user = mUser.get(idx);
            return user;
        }
        return null;
    }
    
    public String add_user(String data[]) {
        TFactory factory = new TFactory();
        TUser user = factory.create_user(data);
        
        if (search_user(user) == null) {
            mUser.add(user);
            System.out.println("Added");
            return user.toString();
        }
        return null;
    }
        
    
}
