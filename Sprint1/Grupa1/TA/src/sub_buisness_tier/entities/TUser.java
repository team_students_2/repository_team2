package sub_buisness_tier.entities;

public class TUser {
    
    //private int ID;
    private String username;
    private String password;
        
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    
    /*
    public int getID() {
        return ID;
    }
    */
    
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /*
    public void setID(int ID) {
        this.ID = ID;
    }
    */

    
    @Override
    public String toString() {
        String USER = "Username : " + getUsername() + ", Password: " + getPassword();
        return USER;
    }

    
    @Override
    public boolean equals(Object obj)
    {
        if(obj == null){
            return false;
        }
        if(!TUser.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        final TUser other = (TUser) obj;                                        //id czy username
        if((this.username == null) ? (other.username != null) : !this.username.equals(other.username)){
            return false;
        }
        return true;
    }

}
