/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier;

import sub_buisness_tier.entities.TAirport;
import sub_buisness_tier.entities.TJourneyType;
import sub_buisness_tier.entities.TUser;

/**
 *
 * @author LukaszLech & WiktorJaworski
 */
public class TFactory {
    
    public TAirport create_airport(String data[]) {
        TAirport airport = null;
        
        airport = new TAirport();
        airport.setCountry(data[0]);
        airport.setCity(data[1]);
        airport.setCode(data[2]);
        
        return airport;
    }
    
        public TJourneyType create_journeytype(String data) {
        TJourneyType journeytype = null;
        journeytype = new TJourneyType();
        journeytype.setJourneyType(data);
   
        return journeytype;
    }
        
    public TUser create_user(String data[]){
        TUser user = null;
        user = new TUser();
        
        user.setUsername(data[0]);
        user.setPassword(data[0]);
        
        return user;
    }    
    
}
