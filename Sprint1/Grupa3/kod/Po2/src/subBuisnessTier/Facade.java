package subBuisnessTier;

import java.util.ArrayList;
import subBuisnessTier.entities.Sport;
import subBuisnessTier.entities.Amenity;
import subBuisnessTier.entities.Hotel;

public class Facade {
    
    ArrayList<Sport> sportList;
    ArrayList<Amenity> amenityList;
    ArrayList<Hotel> hotelList;

    public Facade() {
        sportList = new ArrayList();
        amenityList = new ArrayList();
        hotelList = new ArrayList();
    }

    public ArrayList<Sport> getSportList() {
        return sportList;
    }

    public void setSportList(ArrayList<Sport> sportList) {
        this.sportList = sportList;
    }
    
    public String addSport(String data[]) {
        Factory factory = new Factory();
        Sport sport = factory.createSport(data);
        
        if (searchSport(sport) == null) {
            sportList.add(sport);
            System.out.println("Added");
            return sport.toString();
        }
        return null;
    }
    
    public Sport searchSport(Sport sport) {
        int idx = sportList.indexOf(sport);
        
        if (idx != -1) {
            sport = sportList.get(idx);
            return sport;
        }
        return null;
    }

    public ArrayList<Amenity> getAmenityList() {
        return amenityList;
    }

    public void setAmenityList(ArrayList<Amenity> amenityList) {
        this.amenityList = amenityList;
    }
    
    public String addAmenity(String data[]) {
        Factory factory = new Factory();
        Amenity amenity = factory.createAmenity(data);
        
        if (searchAmenity(amenity) == null) {
            amenityList.add(amenity);
            System.out.println("Added");
            return amenity.toString();
        }
        return null;
    }
    
    public Amenity searchAmenity(Amenity amenity) {
        int idx = amenityList.indexOf(amenity);
        
        if (idx != -1) {
            amenity = amenityList.get(idx);
            return amenity;
        }
        return null;
    }

    public ArrayList<Hotel> getHotelList() {
        return hotelList;
    }

    public void setHotelList(ArrayList<Hotel> hotelList) {
        this.hotelList = hotelList;
    }
    
    public String addHotel(String data[]) {
        Factory factory = new Factory();
        Hotel hotel = factory.createHotel(data);
        
        if (searchHotel(hotel) == null) {
            hotelList.add(hotel);
            System.out.println("Added");
            return hotel.toString();
        }
        return null;
    }
    
    public Hotel searchHotel(Hotel hotel) {
        int idx = hotelList.indexOf(hotel);
        
        if (idx != -1) {
            hotel = hotelList.get(idx);
            return hotel;
        }
        return null;
    }
}
