package subBuisnessTier;

import subBuisnessTier.entities.Sport;
import subBuisnessTier.entities.Amenity;
import subBuisnessTier.entities.Hotel;

public class Factory {
       
    public Sport createSport(String data[]) {
        Sport sport = new Sport();
        
        sport.setType(data[0]);
        sport.setPrice(data[1]);
        
        return sport;
    }
    
    public Amenity createAmenity(String data[]) {
        Amenity amenity = new Amenity();
        
        amenity.setType(data[0]);
        amenity.setPrice(data[1]);
        
        return amenity;
    }
    
    public Hotel createHotel(String data[]) {
        Hotel hotel = new Hotel();
        
        hotel.setName(data[0]);
        hotel.setStreet(data[1]);
        hotel.setPostalCode(data[2]);
        hotel.setCity(data[3]);
        hotel.setStandard(data[4]);
                
        return hotel;
    }
}
