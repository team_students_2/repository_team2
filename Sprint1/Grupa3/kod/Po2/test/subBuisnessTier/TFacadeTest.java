/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package subBuisnessTier;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import subBuisnessTier.entities.Amenity;
import subBuisnessTier.entities.Sport;

/**
 *
 * @author Dawid
 */
public class TFacadeTest {
    
    public TFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSportList method, of class TFacade.
     */
    @Test
    public void testGetSportList() {
        System.out.println("getSportList");
        Facade instance = new Facade();
        ArrayList<Sport> expResult = null;
        ArrayList<Sport> result = instance.getSportList();
        assertEquals(expResult, result);
        
    }

    

    /**
     * Test of addSport method, of class TFacade.
     */
    @Test
    public void testAddSport() {
        System.out.println("addSport");
        String[] data = {"Sport","Cena"};
        Facade instance = new Facade();
        String[] expResult = {"Sport","Cena"};
        String result = instance.addSport(data);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of searchSport method, of class TFacade.
     */
    @Test
    public void testSearchSport() {
        System.out.println("searchSport");
        Sport sport = null;
        Facade instance = new Facade();
        Sport expResult = null;
        Sport result = instance.searchSport(sport);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getAmenityList method, of class TFacade.
     */
    @Test
    public void testGetAmenityList() {
        System.out.println("getAmenityList");
        Facade instance = new Facade();
        ArrayList<Amenity> expResult = null;
        ArrayList<Amenity> result = instance.getAmenityList();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setAmenityList method, of class TFacade.
     */
    @Test
    public void testSetAmenityList() {
        System.out.println("setAmenityList");
        ArrayList<Amenity> amenityList = null;
        Facade instance = new Facade();
        instance.setAmenityList(amenityList);
        
    }

    /**
     * Test of addAmenity method, of class TFacade.
     */
    @Test
    public void testAddAmenity() {
        System.out.println("addAmenity");
        String[] data = null;
        Facade instance = new Facade();
        String expResult = "";
        String result = instance.addAmenity(data);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of searchAmenity method, of class TFacade.
     */
    @Test
    public void testSearchAmenity() {
        System.out.println("searchAmenity");
        Amenity amenity = null;
        Facade instance = new Facade();
        Amenity expResult = null;
        Amenity result = instance.searchAmenity(amenity);
        assertEquals(expResult, result);
        
    }
    
}
