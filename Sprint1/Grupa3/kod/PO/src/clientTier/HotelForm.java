package clientTier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class HotelForm extends JPanel implements ActionListener {
    JLabel nameLbl = new JLabel("Name");
    JTextField nameTxt = new JTextField(30);
    JLabel streetLbl = new JLabel("Street");
    JTextField streetTxt = new JTextField(30);
    JLabel postalCodeLbl = new JLabel("Postal Code");
    JTextField postalCodeTxt = new JTextField(30);
    JLabel cityLbl = new JLabel("City");
    JTextField cityTxt = new JTextField(30);
    JLabel standardLbl = new JLabel("Standard");
    JTextField standardTxt = new JTextField(30);
    JButton addHotel = new JButton("Add hotel");

    public HotelForm() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(nameLbl);
        add(nameTxt);
        add(streetLbl);
        add(streetTxt);
        add(postalCodeLbl);
        add(postalCodeTxt);
        add(cityLbl);
        add(cityTxt);
        add(standardLbl);
        add(standardTxt);
 
        add(addHotel);
    }
    
    public void init() {
        addHotel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String[] data = formHotel();
        if (data == null) {
            return;
        }
        Client.getFacade().addHotel(data);
    }

    public String[] formHotel() {
        if (contentValidate(nameTxt) == null) {
            return null;
        }
        if (contentValidate(streetTxt) == null) {
            return null;
        }
        if (contentValidate(postalCodeTxt) == null) {
            return null;
        }
        if (contentValidate(cityTxt) == null) {
            return null;
        }
        if (contentValidate(standardTxt) == null) {
            return null;
        }

        String data[] = {(String) nameTxt.getText(),
            (String) streetTxt.getText(),
            (String) postalCodeTxt.getText(),
            (String) cityTxt.getText(),
            (String) standardTxt.getText()};
        return data;
    }

    public String contentValidate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
}
