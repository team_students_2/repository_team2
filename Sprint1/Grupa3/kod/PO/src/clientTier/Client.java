package clientTier;

import GUI.PanelUtil;
import subBuisnessTier.Facade;

public class Client {
    static Facade facade = new Facade();

    public static Facade getFacade() {
        return facade;
    }

    public static void setFacade(Facade facade) {
        Client.facade = facade;
    }
    
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(() -> {
           PanelUtil.createAndShowGUI();
       });
    }
    
}
