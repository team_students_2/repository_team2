package clientTier;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class AmenityForm extends JPanel implements ActionListener {
    JLabel typeLbl = new JLabel("Type");
    JTextField typeTxt = new JTextField(30);
    JLabel priceLbl = new JLabel("Price");
    JTextField priceTxt = new JTextField(30);
    JButton addAmenity = new JButton("Add Amenity");

    public AmenityForm() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(typeLbl);
        add(typeTxt);
        add(priceLbl);
        add(priceTxt);
 
        add(addAmenity);
    }

    public void init() {
        addAmenity.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String[] data = formAmenity();
        if (data == null) {
            return;
        }
        Client.getFacade().addSport(data);
    }

    public String[] formAmenity() {
        if (contentValidate(typeTxt) == null) {
            return null;
        }
        if (contentValidate(priceTxt) == null) {
            return null;
        }

        String data[] = {(String) typeTxt.getText(),
            (String) priceTxt.getText()};
        return data;
    }

    public String contentValidate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }    
}
