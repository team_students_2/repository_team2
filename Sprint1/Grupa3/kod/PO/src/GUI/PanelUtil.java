package GUI;

import clientTier.AmenityForm;
import clientTier.Card0;
import clientTier.HotelForm;
import clientTier.SportForm;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class PanelUtil implements ActionListener {

    JPanel cards; //a panel that uses CardLayout
    final static String SPORT = "Sport";
    final static String AMENITY = "Amenity";
    final static String HOTEL = "Hotel"; 
    final static String NOTHING1 = "Empty1";
    
    public JMenuBar createMenuBar() {
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;
        JMenuItem menuItem2;
        JMenuItem menuItem3;

        //Create the menu bar.
        menuBar = new JMenuBar();

        menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_A);
        menuBar.add(menu);

        menuItem = new JMenuItem(SPORT, KeyEvent.VK_T);
        menuItem.setMnemonic(KeyEvent.VK_T); //used constructor instead
        menuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_1, ActionEvent.ALT_MASK));
        menuItem.addActionListener(this);
        menu.add(menuItem);
        
        menuItem2 = new JMenuItem(AMENITY, KeyEvent.VK_T);
        menuItem2.setMnemonic(KeyEvent.VK_T); //used constructor instead
        menuItem2.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_2, ActionEvent.ALT_MASK));
        menuItem2.addActionListener(this);
        menu.add(menuItem2);
        
        menuItem3 = new JMenuItem(HOTEL, KeyEvent.VK_T);
        menuItem3.setMnemonic(KeyEvent.VK_T); //used constructor instead
        menuItem3.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_3, ActionEvent.ALT_MASK));
        menuItem3.addActionListener(this);
        menu.add(menuItem3);
        
        return menuBar;
    }
    
    public Container createContentPane() {
        //Create the content-pane-to-be.

        Card0 card0 = new Card0();
        SportForm card1 = new SportForm();
        AmenityForm card2 = new AmenityForm();
        HotelForm card3 = new HotelForm();
        card1.init();
        card2.init();
        card3.init();

        //Create the panel that contains the "cards".
        cards = new JPanel(new CardLayout());
        cards.add(card0, NOTHING1);
        cards.add(card1, SPORT);
        cards.add(card2, AMENITY);
        cards.add(card3, HOTEL);

        JPanel p1 = new JPanel();

        p1.add(cards, BorderLayout.CENTER);
        return p1;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem) (e.getSource());
        CardLayout cl = (CardLayout) (cards.getLayout());
        switch (source.getText()) {
            case SPORT:
                cl.show(cards, SPORT);
                break;
                
            case AMENITY:
                cl.show(cards, AMENITY);
                break;
                
            case HOTEL:
                cl.show(cards, HOTEL);
                break;
        }
    }
    
    public static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Travel Agency");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(800, 460);
        //Create and set up the content pane.
        PanelUtil demo = new PanelUtil();
        frame.setJMenuBar(demo.createMenuBar());
        frame.setContentPane(demo.createContentPane());

        //Display the window.
        frame.setVisible(true);
    }
}
