/**
 * Created by jm on 05.04.17.
 */
public class Direction {
    private String Country;

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "Country='" + Country + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return Country == ((Direction) obj).getCountry();
    }
}
