/**
 * Created by jm on 29.03.17.
 */
public class Alimentation {
    private String meals;


    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getMeals() {
        return meals;
    }


    @Override
    public String toString() {
        return "Alimentation{" +
                "meals='" + meals + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return meals == ((Alimentation) obj).getMeals();
    }
}
