/**
 * Created by jm on 05.04.17.
 */
public class Period {
    private String stayPeriod;

    public String getStayPeriod() {
        return stayPeriod;
    }

    public void setStayPeriod(String stayPeriod) {
        this.stayPeriod = stayPeriod;
    }

    @Override
    public String toString() {
        return "Period{" +
                "stayPeriod='" + stayPeriod + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return stayPeriod == ((Period) obj).getStayPeriod();
    }
}
