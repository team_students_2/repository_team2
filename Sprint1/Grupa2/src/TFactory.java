/**
 * Created by jm on 20.03.17.
 */
public class TFactory {

    public Office createOffice(String data[]) {
        Office office = null;
        office = new Office();
        office.setCity(data[0]);
        office.setMail(data[1]);

        return office;
    }

    public Alimentation createAlimentation(String data[]) {
        Alimentation alimentation = null;
        alimentation = new Alimentation();
        alimentation.setMeals(data[0]);

        return alimentation;
    }

    public Direction createDirection(String data[]) {
        Direction direction = null;
        direction = new Direction();
        direction.setCountry(data[0]);

        return direction;
    }

    public Period createPeriod(String data[]) {
        Period period = null;
        period = new Period();
        period.setStayPeriod(data[0]);

        return period;
    }




}
