/**
 * Created by jm on 20.03.17.
 */
import java.util.ArrayList;
import java.util.List;
public class TFacade {

    List<Office> officeList;
    List<Alimentation> alimentationList;
    List<Period> periodList;
    List<Direction> directionList;

    public TFacade() {

        officeList = new ArrayList<>();
        alimentationList = new ArrayList<>();
        directionList = new ArrayList<>();
        periodList = new ArrayList<>();
    }

    public List<Office> getOffices() {

        return officeList;
    }

    void setOffices(List<Office> officeList) {

        this.officeList = officeList;
    }

    public List<Period> getPeriodList() {
        return periodList;
    }

    public void setPeriodList(List<Period> periodList) {
        this.periodList = periodList;
    }

    public List<Direction> getDirectionList() {
        return directionList;
    }

    public void setDirectionList(List<Direction> directionList) {
        this.directionList = directionList;
    }

    public List<Alimentation> getAlimentation() {

        return alimentationList;
    }


    void setAlimentation(List<Alimentation> alimentationList) {

        this.alimentationList = alimentationList;
    }

    public Office searchOffice(Office office) {
        int idx = officeList.indexOf(office);
        if(idx!=-1)
            return officeList.get(idx);
        return null;
    }

    public Direction searchDirection(Direction direction) {
        int idx = directionList.indexOf(direction);
        if(idx!=-1)
            return directionList.get(idx);
        return null;
    }

    public Period searchPeriod(Period period) {
        int idx = periodList.indexOf(period);
        if(idx!=-1)
            return periodList.get(idx);
        return null;
    }

    public Alimentation searchAlimentation(Alimentation alimentation) {
        int idx = alimentationList.indexOf(alimentation);
        if(idx!=-1)
            return alimentationList.get(idx);
        return null;
    }

    public String addOffice(String data[]) {
        TFactory factory = new TFactory();
        Office office = factory.createOffice(data);
        if ((searchOffice(office)) == null) {
            officeList.add(office);
            return office.toString();
        }
        return null;
    }

    public String addAlimentation(String data[]) {
        TFactory factory = new TFactory();
        Alimentation alimentation = factory.createAlimentation(data);
        if ((searchAlimentation(alimentation)) == null) {
            alimentationList.add(alimentation);
            return alimentation.toString();
        }
        return null;
    }

    public String addDirection(String data[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(data);
        if ((searchDirection(direction)) == null) {
            directionList.add(direction);
            return direction.toString();
        }
        return null;
    }

    public String addPeriod(String data[]) {
        TFactory factory = new TFactory();
        Period period = factory.createPeriod(data);
        if ((searchPeriod(period)) == null) {
            periodList.add(period);
            return period.toString();
        }
        return null;
    }

    //JUST FOR HELP
    public void printOffices() {
        for (Office off : officeList) {
            System.out.println(off.toString());
        }

    }

    public void printAlimentation() {
        for (Alimentation ali : alimentationList) {
            System.out.println(ali.toString());
        }

    }

    public void printDirection() {
        for (Direction dir : directionList) {
            System.out.println(dir.toString());
        }

    }

    public void printPeriod() {
        for (Period per : periodList) {
            System.out.println(per.toString());
        }

    }




}
