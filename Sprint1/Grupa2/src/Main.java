/**
 * Created by jm on 20.03.17.
 */
public class Main {

    public static void main(String args[]) {
        TFacade application = new TFacade();
        String officeData1[] = {"Wroclaw", "biuro@wroclaw.pl"};
        String officeData2[] = {"Warszawa", "biuro@warszawa.pl"};
        String officeData3[] = {"Lodz", "biuro@lodz.pl"};
        String officeData4[] = {"Lodz", "biuro2@lodz.pl"};

        String alimentationData1[] = {"All Inclusive"};
        String alimentationData2[] = {"3 posilki"};
        String alimentationData3[] = {"sniadanie i obiadokolacje"};
        String alimentationData4[] = {"sniadanie"};
        String alimentationData5[] = {"bez wyzywienia"};



        System.out.println("Zacznijmy od biur!");
        System.out.println("Chyba jeszcze nic tu nie ma...");
        application.printOffices();
        application.addOffice(officeData1);
        System.out.println("\nCzy pierwsze się dodało?");
        application.printOffices();
        application.addOffice(officeData2);
        application.addOffice(officeData3);
        System.out.println("\nPo dodaniu trzech");
        application.printOffices();

        System.out.println("\nSpróbujmy dodać jeszcze raz to samo:");
        application.addOffice(officeData2);
        application.printOffices();

        System.out.println("\nTo moze drugie biuro w Łodzi?");
        application.addOffice(officeData4);
        application.printOffices();


        System.out.println("Czas na wyzywienie:");
        System.out.println("Chyba jeszcze nic tu nie ma...");
        application.printAlimentation();
        application.addAlimentation(alimentationData1);
        System.out.println("\nCzy pierwsze się dodało?");
        application.printAlimentation();
        application.addAlimentation(alimentationData2);
        application.addAlimentation(alimentationData3);
        application.addAlimentation(alimentationData4);
        System.out.println("\nPo dodaniu czterech");
        application.printAlimentation();

        System.out.println("\nSpróbujmy dodać jeszcze raz to samo:");
        application.addAlimentation(alimentationData4);
        application.printAlimentation();


    }
}
