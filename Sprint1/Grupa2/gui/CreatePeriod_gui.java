import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreatePeriod_gui extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreatePeriod_gui frame = new CreatePeriod_gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreatePeriod_gui() {
		setTitle("Stay Period");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblStayPeriod = new JLabel("Input stay period.");
		lblStayPeriod.setBounds(10, 11, 95, 14);
		contentPane.add(lblStayPeriod);
		
		textField = new JTextField();
		textField.setBounds(10, 36, 228, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnApply = new JButton("Apply");
		btnApply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] czas_pobytu = new String[1];
				czas_pobytu[0] = textField.getText();
				
				if (czas_pobytu.equals(""))
					JOptionPane.showMessageDialog(null, "Pole z czasem pobytu nie moze byc puste.", "Nieprawidłowe dane.", JOptionPane.INFORMATION_MESSAGE);
				else
					Client.getFacade().addPeriod(czas_pobytu);
			}
		});
		btnApply.setBounds(335, 227, 89, 23);
		contentPane.add(btnApply);
	}
}
