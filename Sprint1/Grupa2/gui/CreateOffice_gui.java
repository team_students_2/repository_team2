package createOffice_gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

public class CreateOffice_gui extends JFrame {

	private JPanel contentPane;
	private JTextField tfCity;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateOffice_gui frame = new CreateOffice_gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateOffice_gui() {
		setTitle("Create Office");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCreateOffice = new JButton("Create Office");
		btnCreateOffice.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCreateOffice.setBounds(300, 227, 124, 23);
		contentPane.add(btnCreateOffice);
		
		JLabel lblCity = new JLabel("City");
		lblCity.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCity.setBounds(10, 11, 46, 14);
		contentPane.add(lblCity);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblMail.setBounds(10, 36, 46, 14);
		contentPane.add(lblMail);
		
		tfCity = new JTextField();
		tfCity.setBounds(66, 9, 150, 20);
		contentPane.add(tfCity);
		tfCity.setColumns(10);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(66, 34, 150, 20);
		contentPane.add(textField);
	}
}
