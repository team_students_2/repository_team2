import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreateDirection_gui extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateDirection_gui frame = new CreateDirection_gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateDirection_gui() {
		setTitle("Destination");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPodajMiejsceDocelowe = new JLabel("Input destination");
		lblPodajMiejsceDocelowe.setBounds(10, 11, 180, 14);
		contentPane.add(lblPodajMiejsceDocelowe);
		
		textField = new JTextField();
		textField.setBounds(10, 36, 227, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnApply = new JButton("Apply");
		btnApply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] direction = new String[1];
				direction[0] = textField.getText();
				
				if (direction.equals(""))
					JOptionPane.showMessageDialog(null, "Pole z kierunkiem podróży nie moze byc puste.", "Nieprawidłowe dane.", JOptionPane.INFORMATION_MESSAGE);
				else
					Client.getFacade().addDirection(direction);
			}
		});
		btnApply.setBounds(335, 227, 89, 23);
		contentPane.add(btnApply);
	}

}
