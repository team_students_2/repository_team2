import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreateAlimentation_gui extends JFrame {

	private JPanel contentPane;
	private JTextField tfAddMeal;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreateAlimentation_gui frame = new CreateAlimentation_gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateAlimentation_gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		DefaultListModel<String> model = new DefaultListModel<>();
		JList<String> listMeals = new JList<String>(model);
		listMeals.setBounds(10, 11, 143, 239);
		contentPane.add(listMeals);
		
		tfAddMeal = new JTextField();
		tfAddMeal.setBounds(163, 9, 158, 20);
		contentPane.add(tfAddMeal);
		tfAddMeal.setColumns(10);
		
		JButton btnAddMeal = new JButton("Add Meal");
		btnAddMeal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String newMeal = tfAddMeal.getText();
				
				if(newMeal.equals(""))
					JOptionPane.showMessageDialog(null, "Pole z nazwa posilku nie moze byc puste.", "Nieprawidłowe dane.", JOptionPane.INFORMATION_MESSAGE);
				else
					model.addElement(newMeal);
			}
		});
		btnAddMeal.setBounds(331, 8, 89, 23);
		contentPane.add(btnAddMeal);
		
		JButton btnCreateAlimentation = new JButton("Create Alimentation");
		btnCreateAlimentation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client.getFacade().addAlimentation((String[])model.toArray());
			}
		});
		btnCreateAlimentation.setBounds(284, 227, 136, 23);
		contentPane.add(btnCreateAlimentation);
	}
}
