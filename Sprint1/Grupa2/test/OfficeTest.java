
import org.junit.Test;
import static org.junit.Assert.*;

public class OfficeTest {
    
    public OfficeTest() {
    }

    /**
     * Test of setMail and getMail methods, of class Office.
     */
    @Test
    public void testGetSetMail() {
        System.out.println("setMail");
        String mail = "abcd@mail.com";
        Office instance = new Office();
        instance.setMail(mail);
        
        assertEquals(instance.getMail(), mail);
    }

    /**
     * Test of setCity and getCity methods, of class Office.
     */
    @Test
    public void testGetSetCity() {
        System.out.println("setCity");
        String city = "Warsaw";
        Office instance = new Office();
        instance.setCity(city);
        
        assertEquals(instance.getCity(), city);
    }
    
}
