/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.agency;

import client_tier.Client;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Damian
 */
public class Travel_form extends JPanel implements ActionListener {
    JLabel usernameLbl = new JLabel("Username");
    JTextField username = new JTextField(30);
    JLabel passwordLbl = new JLabel("Password");
    JPasswordField password = new JPasswordField(30);
    
    JLabel directionLbl = new JLabel("Direction");
    DirectionTableModel directionTableModel = new DirectionTableModel();
    JTable directionTable = new JTable(directionTableModel);
    
    JLabel officeLbl = new JLabel("Office");
    OfficeTableModel officeTableModel = new OfficeTableModel();
    JTable officeTable = new JTable(officeTableModel);
    
    JLabel hotelLbl = new JLabel("Hotel");
    HotelTableModel hotelTableModel = new HotelTableModel();
    JTable hotelTable = new JTable(hotelTableModel);
    
    JLabel alimentationLbl = new JLabel("Alimentation");
    AlimentationTableModel alimentationTableModel = new AlimentationTableModel();
    JTable alimentationTable = new JTable(alimentationTableModel);
    
    JLabel sportLbl = new JLabel("Sport");
    SportTableModel sportTableModel = new SportTableModel();
    JTable sportTable = new JTable(sportTableModel);
    
    JLabel startDateLbl = new JLabel("Start Date");
    JTextField startDate = new JTextField(30);
    JLabel endDateLbl = new JLabel("End Date");
    JTextField endDate = new JTextField(30);
    
    JLabel journeyTypeLbl = new JLabel("Journey Type");
    JTextField journeyType = new JTextField(30);
    
    JButton add_travel = new JButton("Add Travel");
    
    public Travel_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(usernameLbl);
        add(username);
        add(passwordLbl);
        add(password);
        
        add(directionLbl);
        directionTable.setPreferredScrollableViewportSize(new Dimension(500, 50));
        directionTable.setFillsViewportHeight(true);
        directionTable.getSelectionModel().addListSelectionListener(new DirectionRowListener());
        add(new JScrollPane(directionTable));
        
        add(officeLbl);
        officeTable.setPreferredScrollableViewportSize(new Dimension(500, 50));
        officeTable.setFillsViewportHeight(true);
        add(new JScrollPane(officeTable));
        
        add(hotelLbl);
        hotelTable.setPreferredScrollableViewportSize(new Dimension(500, 50));
        hotelTable.setFillsViewportHeight(true);
        hotelTable.getSelectionModel().addListSelectionListener(new HotelRowListener());
        add(new JScrollPane(hotelTable));
        
        add(alimentationLbl);
        alimentationTable.setPreferredScrollableViewportSize(new Dimension(500, 50));
        alimentationTable.setFillsViewportHeight(true);
        add(new JScrollPane(alimentationTable));
        
        add(sportLbl);
        sportTable.setPreferredScrollableViewportSize(new Dimension(500, 50));
        sportTable.setFillsViewportHeight(true);
        add(new JScrollPane(sportTable));
        
        add(startDateLbl);
        add(startDate);
        add(endDateLbl);
        add(endDate);
        
        add(journeyTypeLbl);
        add(journeyType);
        
        add(add_travel);
    }
    
     public void init() {
        table_content();
        add_travel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!directionTable.getSelectionModel().isSelectionEmpty()) {
            if (!officeTable.getSelectionModel().isSelectionEmpty()) {
                if (!hotelTable.getSelectionModel().isSelectionEmpty()) {
                    if (!alimentationTable.getSelectionModel().isSelectionEmpty()) {
                        if (!sportTable.getSelectionModel().isSelectionEmpty()) {
                            String[] userData = getUserData();
                            if (userData == null) {
                                return;
                            }
                            String[] periodData = getPeriodData();
                            if (periodData == null) {
                                return;
                            }
                            String[] journeyTypeData = getJourneyTypeData();
                            if (journeyTypeData == null) {
                                return;
                            }
                            Client.getFacade().clientOffer(userData, direction(), hotel(), alimentation(), sport(), journeyTypeData, periodData, office());
                        }
                    }
                    
                
                }
            }
        }
    }
    
    private String[] getUserData() {
        if (content_validate(username) == null) {
            return null;
        }
        if (content_validate(password) == null) {
            return null;
        }
        String cPasswd = new String(password.getPassword());
        String data[] = {username.getText(), cPasswd};
        return data;
    }
    
    private String[] getPeriodData() {
        if (content_validate(startDate) == null) {
            return null;
        }
        if (content_validate(endDate) == null) {
            return null;
        }
        return new String[] {"", startDate.getText(), endDate.getText()};
    }
    
    private String[] getJourneyTypeData() {
        if (content_validate(journeyType) == null) {
            return null;
        }
        return new String[] {journeyType.getText()};
    }
    
    private Object content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
    
    void table_content() {
        Object[][] directions = Client.getFacade().getDirection();
        directionTableModel.setData(directions);
        directionTable.repaint();
        
        Object[][] offices = Client.getFacade().getOffice();
        officeTableModel.setData(offices);
        officeTable.repaint();
        
        String directionData[] = {(String) directionTableModel.getValueAt(0, 0)};
        Object[][] hotels = Client.getFacade().getHotel(directionData);//blad
        hotelTableModel.setData(hotels);
        hotelTable.repaint();
        
        String directionData2[] = {(String) directionTableModel.getValueAt(0, 0)};
        String hotelData[] = {(String) hotelTableModel.getValueAt(0, 0), (String) hotelTableModel.getValueAt(0, 1), (String) hotelTableModel.getValueAt(0, 2), (String) hotelTableModel.getValueAt(0, 3), (String) hotelTableModel.getValueAt(0, 4)};
        Object[][] alimentations = Client.getFacade().getAlimentation(directionData2, hotelData);
        alimentationTableModel.setData(alimentations);
        alimentationTable.repaint();
        
        String directionData3[] = {(String) directionTableModel.getValueAt(0, 0)};
        String hotelData2[] = {(String) hotelTableModel.getValueAt(0, 0), (String) hotelTableModel.getValueAt(0, 1), (String) hotelTableModel.getValueAt(0, 2), (String) hotelTableModel.getValueAt(0, 3), (String) hotelTableModel.getValueAt(0, 4)};
        Object[][] sports = Client.getFacade().getSport(directionData3, hotelData2);
        sportTableModel.setData(sports);
        sportTable.repaint();
    }
    
    private String[] direction() {
        String data[] = {(String) directionTableModel.getValueAt(directionTable.getSelectedRow(), 0)};
        return data;
    }
    
    private String[] office() {
        String data[] = {(String) officeTableModel.getValueAt(officeTable.getSelectedRow(), 0), (String) officeTableModel.getValueAt(officeTable.getSelectedRow(), 1)};
        return data;
    }
    
    private String[] hotel() {
        String data[] = {(String) hotelTableModel.getValueAt(hotelTable.getSelectedRow(), 0), (String) hotelTableModel.getValueAt(hotelTable.getSelectedRow(), 1), (String) hotelTableModel.getValueAt(hotelTable.getSelectedRow(), 2), (String) hotelTableModel.getValueAt(hotelTable.getSelectedRow(), 3), (String) hotelTableModel.getValueAt(hotelTable.getSelectedRow(), 4)};
        return data;
    }
    
    private String[] alimentation() {
        String data[] = {(String) alimentationTableModel.getValueAt(alimentationTable.getSelectedRow(), 0), (String) alimentationTableModel.getValueAt(alimentationTable.getSelectedRow(), 1)};
        return data;
    }
    
    private String[] sport() {
        String data[] = {(String) sportTableModel.getValueAt(sportTable.getSelectedRow(), 0), (String) sportTableModel.getValueAt(sportTable.getSelectedRow(), 1)};
        return data;
    }
    
    private class TableModel extends AbstractTableModel {
        private String[] columnNames;
        private Object[][] data;
        
        protected void setColumnNames(String[] columnNames) {
            this.columnNames = columnNames;
        }
        
        public void setData(Object[][] val) {
            data = val;
        }
        
        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public int getRowCount() {
            return data.length;
        }

        @Override
        public String getColumnName(int col) {
            return columnNames[col];
        }

        @Override
        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        @Override
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        @Override
        public boolean isCellEditable(int row, int col) {
            return col < 0;
        }

        @Override
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }
    
    
    private class DirectionTableModel extends TableModel {
        public DirectionTableModel() {
            setColumnNames(new String[] {"Country"});
        } 
    }
    
    private class OfficeTableModel extends TableModel {
        public OfficeTableModel() {
            setColumnNames(new String[] {"Mail", "City"});
        }        
    }
    
    private class HotelTableModel extends TableModel {
        public HotelTableModel() {
            setColumnNames(new String[] {"Name", "Street", "Postal Code", "City", "Standard"});
        }     
    }
    
    private class AlimentationTableModel extends TableModel {
        public AlimentationTableModel() {
            setColumnNames(new String[] {"Meals", "Price"});
        }
    }
    
    private class SportTableModel extends TableModel {
        public SportTableModel() {
            setColumnNames(new String[] {"Type", "Price"});
        }     
    }
    
    private class DirectionRowListener implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                return;
            }
            Object[][] hotels = Client.getFacade().getHotel(direction());
            hotelTableModel.setData(hotels);
            hotelTable.repaint();
        }
    }
    
    private class HotelRowListener implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                return;
            }
            Object[][] alimentations = Client.getFacade().getAlimentation(direction(), hotel());
            alimentationTableModel.setData(alimentations);
            alimentationTable.repaint();
        
            Object[][] sports = Client.getFacade().getSport(direction(), hotel());
            sportTableModel.setData(sports);
            sportTable.repaint();
        }
    }
}
