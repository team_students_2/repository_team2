package sub_buisness_tier.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Office {
    private String mail;
    private String city;
    private List<Travel> travels;
    
    public Office()
    {
        travels = new ArrayList<Travel>();
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void createTravel(TUser user, Direction direction, Object[] hotelAlimentationSport, Object[] airportPeriod, String typ_wyjazduData[])
    {
        Travel travel = new Travel(this, user, (Period) airportPeriod[1], direction, (Hotel) hotelAlimentationSport[0], (AirPort) airportPeriod[0], (Alimentation) hotelAlimentationSport[1], (SportEntertainment) hotelAlimentationSport[2], typ_wyjazduData);
        addTravel(travel);
        user.addTravel(travel);
    }
    
    public void Apriori(int min_sup){
        
        int count = travels.size();
    }

    @Override
    public String toString() {
        return "Office{" +
                "mail='" + mail + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public String[] toString_() {
        return new String[] {getMail(), getCity()};
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.mail);
        hash = 23 * hash + Objects.hashCode(this.city);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Office other = (Office) obj;
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        return true;
    }
    
    private void addTravel(Travel travel) {
        if(!travels.contains(travel)) {
            travels.add(travel);
        }
    }
}