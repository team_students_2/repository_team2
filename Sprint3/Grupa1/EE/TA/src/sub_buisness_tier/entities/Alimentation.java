package sub_buisness_tier.entities;

import java.util.Objects;

public class Alimentation {
    protected String meals;
    protected int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getMeals() {
        return meals;
    }


    @Override
    public String toString() {
        return "Alimentation{" +
                "meals='" + meals + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.meals);
        hash = 23 * hash + this.price;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alimentation other = (Alimentation) obj;
        if (!Objects.equals(this.meals, other.meals)) {
            return false;
        }
        if (this.price != other.price) {
            return false;
        }
        return true;
    }

    
    
    public String[] toString_() {
        return new String[] {getMeals(), Integer.toString(getPrice())};
    }
}