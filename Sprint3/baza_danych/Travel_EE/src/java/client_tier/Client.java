package client_tier;

//import sub_buisness_tier.TFacade;

import business_tier_ejb.Facade_ejbRemote;
import javax.ejb.EJB;
import travel.agency.Panel_util;


public class Client {

    @EJB
    private static Facade_ejbRemote facade;

   
       
 //  static TFacade facade = new TFacade();
    

    static public Facade_ejbRemote getFacade() {
        return facade; }

    static public void setFacade(Facade_ejbRemote facade) {
        Client.facade = facade; }
    
    public static void main(String[] args) {
       java.awt.EventQueue.invokeLater(() -> {
           Panel_util.createAndShowGUI();
       });
    } 
}
