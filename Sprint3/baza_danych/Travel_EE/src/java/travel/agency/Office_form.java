/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author LukaszLech
 */
public class Office_form extends JPanel implements ActionListener {
    
    JLabel lmail = new JLabel("Mail");
    JTextField mail = new JTextField(30);
    JLabel lcity = new JLabel("City");
    JTextField city = new JPasswordField(30);
    JButton add_office = new JButton("Add Office");

    public Office_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lmail);
        add(mail);
        add(lcity);
        add(city);
        add(add_office);
    }
    
    public void init() {
        add_office.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] data = form_addOffice();
        if (data == null) {
            return;
        }
        Client.getFacade().addOffice(data);
    }

    private String[] form_addOffice() {
        if (content_validate(mail) == null) {
            return null;
        } 
        if (content_validate(city) == null) {
            return null;
        }
        String data[] = {mail.getText(), city.getText()};
        return data;
    }

    private Object content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
    
    
}
