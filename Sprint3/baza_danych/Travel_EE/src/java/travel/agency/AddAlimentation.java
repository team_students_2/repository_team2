/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Dawid
 */
public class AddAlimentation extends JPanel implements ActionListener {

    JLabel lmeal = new JLabel("Meals");
    JTextField meal = new JTextField(30);
    JLabel lprice = new JLabel("Price");
    JPasswordField price = new JPasswordField(30);
    JButton add_alimentation = new JButton("Add");

    AddAlimentation() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lmeal);
        add(meal);
        add(lprice);
        add(price);
        add(add_alimentation);
    }

    public void init() {
        add_alimentation.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String[] data = form_addalimentation();
        if (data == null) {
            return;
        }
        Client.getFacade().add_alimentation(data);
        Client.getFacade().addToDatabase();
        }

    public String[] form_addalimentation() {
        if (content_validate(meal) == null) {
            return null;
        }
        if (content_validate(price) == null) {
            return null;
        }
        String cPrice = new String(price.getPassword());
        String data[] = {meal.getText(), cPrice};
        return data;
    }

    public String content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }

}
