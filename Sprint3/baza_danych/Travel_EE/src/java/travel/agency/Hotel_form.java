package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
//import sub_buisness_tier.entities.Direction;
//import sub_buisness_tier.entities.Hotel;

/**
 *
 * @author LukaszLech
 */
public class Hotel_form extends JPanel implements ActionListener{
    JLabel lname = new JLabel("Name");
    JTextField name = new JTextField(30);
    JLabel lstreet = new JLabel("Street");
    JTextField street = new JPasswordField(30);
    JLabel lpostalCode = new JLabel("Postal Code");
    JTextField postalCode = new JPasswordField(30);
    JLabel lcity = new JLabel("City");
    JTextField city = new JPasswordField(30);
    JLabel lstandard = new JLabel("Standard");
    JTextField standard = new JPasswordField(30);
    
    JComboBox<String[]> cbdirection = new JComboBox<>();
   // JComboBox<Direction> cbdirection = new JComboBox<>();
    
    JButton add_hotel = new JButton("Add Hotel");

    public Hotel_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lname);
        add(name);
        add(lstreet);
        add(street);
        add(lpostalCode);
        add(postalCode);
        add(lcity);
        add(city);
        add(lstandard);
        add(standard);
        add(cbdirection);  
        add(add_hotel);
    }
    
    
    
    public void init() {
        add_hotel.addActionListener(this);
//        String[] data = {"test"};
//        String[] data2 = {"test2"};
//        Client.getFacade().addDirection(data);
//        Client.getFacade().addDirection(data2);
        /*
        for(String[] directiondata: Client.getFacade().getDirectionDataList()) {
            cbdirection.addItem(directiondata);
        }
        */
        for(String[] direction: Client.getFacade().getDirectionDataList()) {
            cbdirection.addItem(direction);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] data = form_addHotel();
        if (data == null) {
            return;
        }
        String[] direction = (String[]) cbdirection.getSelectedItem();
        Client.getFacade().addHotel(data, direction);
    }

    private String[] form_addHotel() {
        if (content_validate(name) == null) {
            return null;
        } 
        if (content_validate(street) == null) {
            return null;
        }
        if (content_validate(postalCode) == null) {
            return null;
        }
        if (content_validate(city) == null) {
            return null;
        }
        if (content_validate(standard) == null) {
            return null;
        }
        String data[] = {name.getText(), street.getText(), postalCode.getText(), city.getText(), standard.getText()};
        return data;
    }

    private Object content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
    
    
}
