package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;


public class JourneyType_form extends JPanel implements ActionListener {

    String[] journeytypelist = {"Wakacje samolotowe", "Wakacje samochodowe", "Wycieczka objazdowa", "Narty", "Egzotyka" };
    String selectedtype = "";
    JLabel lcountry = new JLabel("Journey Type");
    JTextField country = new JTextField(30);
    JComboBox journeytype = new JComboBox(journeytypelist);
    JButton add_journeytype = new JButton("Add journey type");

    JourneyType_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lcountry);
        add(journeytype);
        add(add_journeytype);
        
    }

    public void init() {
        add_journeytype.addActionListener(this);
        journeytype.setSelectedItem(null);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        String data = form_journeytype();
        if (data == null) {
            return;
        }
//        Client.getFacade().add_journeytype(data);
    }

    public String form_journeytype() {
        if (content_validate(journeytype) == null) {
            return null;
        }
        String data = journeytype.getSelectedItem().toString();
        return data;
    }

    public String content_validate(JComboBox combo) {
        if (combo.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "selection required");
            return null;
        } else {
            return combo.getSelectedItem().toString();
        }
    }
}


