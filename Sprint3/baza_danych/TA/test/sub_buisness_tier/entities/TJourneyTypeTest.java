/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier.entities;

import org.junit.Test;
import static org.junit.Assert.*;


public class TJourneyTypeTest {
    
    public TJourneyTypeTest() {
    }

    /**
     * Test of getJourneyType method, of class TJourneyType.
     */
    @Test
    public void testGetJourneyType() {
        System.out.println("getJourneyType");
        TJourneyType instance = new TJourneyType();
        instance.setJourneyType("Inferno");
        String expResult = "Inferno";
        String result = instance.getJourneyType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJourneyType method, of class TJourneyType.
     */
    @Test
    public void testSetJourneyType() {
        System.out.println("setJourneyType");
        String journeytype = "reltih";
        TJourneyType instance = new TJourneyType();
        instance.setJourneyType(journeytype);
        assertEquals(journeytype, instance.getJourneyType());
    }

    /**
     * Test of toString method, of class TJourneyType.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TJourneyType instance = new TJourneyType();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
