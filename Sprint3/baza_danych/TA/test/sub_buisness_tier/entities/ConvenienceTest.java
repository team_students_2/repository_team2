/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier.entities;

import org.junit.Test;
import static org.junit.Assert.*;


public class ConvenienceTest {
    
    public ConvenienceTest() {
    }

    /**
     * Test of getType method, of class Convenience.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Convenience instance = new Convenience();
        String expResult = "Random";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setType method, of class Convenience.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "Random";
        Convenience instance = new Convenience();
        instance.setType(type);
        assertEquals(instance.getType(),type);
    }

    /**
     * Test of getPrice method, of class Convenience.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        Convenience instance = new Convenience();
        int expResult = 1337;
        instance.setPrice(expResult);
        int result = instance.getPrice();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPrice method, of class Convenience.
     */
    @Test
    public void testSetPrice() {
        System.out.println("setPrice");
        int price = 1337;
        Convenience instance = new Convenience();
        instance.setPrice(price);
        assertEquals(price, instance.getPrice());
    }
    
}
