package sub_buisness_tier.entities;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by jm on 19.04.17.
 */
@Entity
public class Alimentation {
    protected String meals;
    protected int price;
     private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getMeals() {
        return meals;
    }


    @Override
    public String toString() {
        return "Alimentation{" +
                "meals='" + meals + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return meals.equals(((Alimentation) obj).getMeals());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.meals);
        hash = 67 * hash + this.price;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }
}