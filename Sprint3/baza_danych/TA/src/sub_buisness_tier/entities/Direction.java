package sub_buisness_tier.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import sub_buisness_tier.TFactory;

/**
 * Created by jm on 19.04.17.
 */
@Entity
public class Direction implements Serializable{
        static final long serialVersionUID = 1L;
    protected String Country;
   // @Transient
    @OneToMany(mappedBy="direction", cascade=CascadeType.ALL)
    protected List<Hotel> hotelList;
    @Transient
    private AirPort airPort;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Direction() {
        hotelList = new ArrayList<>();
    }
/////////////////////////////////////////////////////////////////////////////////////////////

//HOTEL HANDLING
    public void addHotel(String Data[]){
        TFactory factory = new TFactory();
        Hotel hotel = factory.createHotel(Data);
        if ((searchHotel(hotel)) == null) {
            hotelList.add(hotel);
            System.out.println("Dodano hotel" + hotel);


        }
        else
            System.out.println(hotel + "jest juz dodany");
    }

    public Hotel searchHotel(Hotel hotel) {
        int idx = hotelList.indexOf(hotel);
        if (idx != -1)
            return hotelList.get(idx);
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

//ALIMENTATION HANDLING
    public void addAlimentation(String data[], String hoteldata[]){
        TFactory factory = new TFactory();
        Hotel hotel = factory.createHotel(hoteldata);
        hotel = searchHotel(hotel);
        if (hotel != null){
            hotel.addAlimentation(data);
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT HANDLING
    public void addAirport (String Data[]){
        TFactory factory = new TFactory();
        AirPort airPort = factory.create_airport(Data);
        setAirPort(airPort);
        System.out.println("Przypisano lotnisko" + airPort);

    };



///////////////////////////////////////////////////////////////////////////////////////////

//SPORT AND ENTERTAINMENT HANDLING
    public void addSportEntertainment(String data[], String hoteldata[]){
        TFactory factory = new TFactory();
        Hotel hotel = factory.createHotel(hoteldata);
        hotel = searchHotel(hotel);
        if (hotel != null){
            hotel.addSportEntertainment(data);
        }

    }
//GETTERS AND SETTERS
    public String getCountry() {
        return Country;
    }
    public String[] getData() {
        String data[] = {getCountry()};
        return data;
    }
    public void setCountry(String country) {
        Country = country;
    }

    public AirPort getAirPort() {
        return airPort;
    }

    public void setAirPort(AirPort airPort) {
        this.airPort = airPort;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "Country='" + Country + '\'' +
                '}';
    }

    /* wtf?
    @Override
    public boolean equals(Object obj) {
        return Country == ((Direction) obj).getCountry();
    }
    */
    
    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(!Direction.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        final Direction other = (Direction) obj;                                      
        if((this.getCountry() == null) ? (other.getCountry() != null) : !this.getCountry().equals(other.getCountry())){
            return false;
        }
        return true;
    }
    


    



}