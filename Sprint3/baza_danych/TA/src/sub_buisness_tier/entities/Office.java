package sub_buisness_tier.entities;

/**
 * Created by jm on 19.04.17.
 */
public class Office {
    private String mail;
    private String city;


    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }



    @Override
    public String toString() {
        return "Office{" +
                "mail='" + mail + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        return mail == ((Office) obj).getMail();
    }
}