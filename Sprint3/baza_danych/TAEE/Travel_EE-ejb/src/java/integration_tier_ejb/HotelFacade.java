/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integration_tier_ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sub_buisness_tier.entities.Hotel;

/**
 *
 * @author dawid
 */
@Stateless
public class HotelFacade extends AbstractFacade<Hotel> implements HotelFacadeLocal {

    @PersistenceContext(unitName = "Travel_EE-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HotelFacade() {
        super(Hotel.class);
    }
    
}
