/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integration_tier_ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import sub_buisness_tier.entities.Direction;

/**
 *
 * @author dawid
 */
@Stateless
public class DirectionFacade extends AbstractFacade<Direction> implements DirectionFacadeLocal {

    @PersistenceContext(unitName = "Travel_EE-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DirectionFacade() {
        super(Direction.class);
    }
    
}
