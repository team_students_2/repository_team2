/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package integration_tier_ejb;

import java.util.List;
import javax.ejb.Local;
import sub_buisness_tier.entities.Alimentation;

/**
 *
 * @author dawid
 */
@Local
public interface AlimentationFacadeLocal {

    void create(Alimentation alimentation);

    void edit(Alimentation alimentation);

    void remove(Alimentation alimentation);

    Alimentation find(Object id);

    List<Alimentation> findAll();

    List<Alimentation> findRange(int[] range);

    int count();
    
}
