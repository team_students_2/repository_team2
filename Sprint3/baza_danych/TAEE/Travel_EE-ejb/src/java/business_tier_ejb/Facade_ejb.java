/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business_tier_ejb;

import integration_tier_ejb.AlimentationFacadeLocal;
import integration_tier_ejb.DirectionFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import sub_buisness_tier.TFacade;
import sub_buisness_tier.entities.Alimentation;
import sub_buisness_tier.entities.Direction;

/**
 *
 * @author PWR
 */
@Stateless
public class Facade_ejb implements Facade_ejbRemote {

    public AlimentationFacadeLocal getAlimentationFacade() {
        return alimentationFacade;
    }

    public void setAlimentationFacade(AlimentationFacadeLocal alimentationFacade) {
        this.alimentationFacade = alimentationFacade;
    }

    public DirectionFacadeLocal getDirectionFacade() {
        return directionFacade;
    }

    public void setDirectionFacade(DirectionFacadeLocal directionFacade) {
        this.directionFacade = directionFacade;
    }

    @EJB
    private AlimentationFacadeLocal alimentationFacade;
    
    @EJB
    private DirectionFacadeLocal directionFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    TFacade facade = new TFacade();
    
    @PostConstruct
    public void init(){
        try{
            getFromDatabase();
        } catch(Exception e){}
    }
    
    
    ///////////////
    public void getFromDatabase(){
        List<Alimentation> a = alimentationFacade.findAll();
        List<Direction> d = directionFacade.findAll();
        facade.alimentationsFromDatabase(a);
        facade.directionsFromDatabase(d);
    }
        
    public void addToDatabase(){
        for (Alimentation a : facade.getAlimentationList()){
            Long id = a.getId();
            if(id == null || alimentationFacade.find(a.getId()) == null){
                alimentationFacade.create(a);
            }
        }
        for (Direction d : facade.getDirectionList()){
           Long id = d.getId();
            if(id == null || directionFacade.find(d.getId()) == null){
                directionFacade.create(d);
            }
    }}
    /////////////////
    
    
    public String add_user(String data[]) {
       return facade.add_user(data);
    }

    
    public String add_alimentation(String data[]){
        return facade.add_alimentation(data);
    }

//OFFICE HANDLING

    public String addOffice(String data[]) {
       return facade.addOffice(data);
    }

  

//DIRECTION HANDLING
    public String addDirection(String data[]) {
        return facade.addDirection(data);
    }

  
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT ADDING
    public String addAirport(String data[], String directiondata[]){
      return facade.addAirport(data, directiondata);
    }


//ALIMENTATION ADDING
    public String addAlimentation(String data[], String directiondata[], String hoteldata[]) {
       return facade.addAlimentation(data, directiondata, hoteldata);
    }


//HOTEL ADDING
    public String addHotel(String data[], String directiondata[]){
       return facade.addHotel(data, directiondata);
    }

//SPORT AND ENTERTAINMENT ADDING
    public String addSportEntertainment(String data[], String directiondata[], String hoteldata[]){
       return facade.addSportEntertainment(data, directiondata, hoteldata);
    }

    public List<String[]> getDirectionDataList() {
      return facade.getDirectionDataList();
    }

}
