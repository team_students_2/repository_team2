/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business_tier_ejb;

import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author PWR
 */
@Remote
public interface Facade_ejbRemote {
        ////////////////////////////////////////////////////////////////////////////////////////////
    //AIRPORT ADDING
    String addAirport(String[] data, String[] directiondata);

    //ALIMENTATION ADDING
    String addAlimentation(String[] data, String[] directiondata, String[] hoteldata);

    //DIRECTION HANDLING
    String addDirection(String[] data);

    //HOTEL ADDING
    String addHotel(String[] data, String[] directiondata);

    //OFFICE HANDLING
    String addOffice(String[] data);

    //SPORT AND ENTERTAINMENT ADDING
    String addSportEntertainment(String[] data, String[] directiondata, String[] hoteldata);

    String add_user(String[] data);
    
    String add_alimentation(String[] data);

    List<String[]> getDirectionDataList();

    public void addToDatabase();

 
    


 
}
