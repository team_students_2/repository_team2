package sub_buisness_tier.entities;

import java.util.ArrayList;
import java.util.List;

public class AirPort {

    private String country;
    private String city;
    private String code;
    private Direction flightDirection;
    private List<TFlight> rozkladLotow;

    public AirPort()
    {
        rozkladLotow = new ArrayList<TFlight>();
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getCode() {
        return code;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {

        String fullName = getCity() + ", " + getCountry() + " - " + getCode();

        return fullName;
    }
    
    public boolean checkFlight(Period period)
    {
        //dla kazdego lotu sprawdzamy czy odbywa sie w odpowiednie dni oraz czy sa wolne miejsca
        for (TFlight f : rozkladLotow)
        {
            if(period.getM_start().equals(f.getStart()) && period.getM_end().equals(f.getEnd()) && f.getNumRemainingSeats() > 0)
                return true;
        }
        
        //jesli nic nie znajdziemy zwracamy falsz
         System.out.println("Nie znaleziono takiego lotu");
        return false;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null){
            return false;
        }
        if(!AirPort.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        final AirPort other = (AirPort) obj;
        if((this.code == null) ? (other.code != null) : !this.code.equals(other.code)){
            return false;
        }
        return true;
    }

}