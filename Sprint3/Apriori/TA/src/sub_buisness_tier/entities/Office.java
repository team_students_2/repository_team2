package sub_buisness_tier.entities;

import java.util.ArrayList;
import java.util.List;
import sub_buisness_tier.entities.Travel;

public class Office {
    private String mail;
    private String city;
    protected List<Travel> travels;
    
    public Office()
    {
        travels = new ArrayList<Travel>();
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void createTravel(TUser user, Direction direction, Object[] hotelAlimentationSport, Object[] airportPeriod, String typ_wyjazduData[])
    {
        Travel travel = new Travel(this, user, (Period) airportPeriod[1], direction, (Hotel) hotelAlimentationSport[0], (AirPort) airportPeriod[0], (Alimentation) hotelAlimentationSport[1], (SportEntertainment) hotelAlimentationSport[2], typ_wyjazduData[0]);
        addTravel(travel);
        user.addTravel(travel);
    }
    
    public void Apriori(Direction original, double min_sup){
        
        //List<Travel> directions = new ArrayList<Travel>();
        List<String[]> dane = new ArrayList<String[]>();
        List<String> frequentItems = new ArrayList<String>();
        List<Integer> itemFrequency = new ArrayList<Integer>();
        int size = 0;
        for(Travel t : travels)
        {
            if (equalDirection(t.getM_direction(),original))
            {
                //directions.add(t);
                //System.out.println("Dodano travel");
                size++;
                String data[] = t.getDane();
                //data[0]= t.getM_direction().getCountry();
                //data[1]= t.getM_airport().getCity();
                //data[2]= t.getM_hotel().getName();
                //data[3]= t.getM_hotel().getStandard();
                //data[4]= t.getAlimentation().meals;
                //data[5]= t.getSport().type;
                //data[6]= t.getM_period().getM_days();
                //data[7]= t.getType();
                dane.add(data);
            }
        }
        //int size = directions.size();
        System.out.println("Znaleziono " + Integer.toString(size) + " Travel z zadanymi danymi");
        int counter;
        double rate;
        for(int m = 0; m <= 5 ; m++)
        {
            for(int i = 0; i < size ; i++)
            {
                counter = 0;
                String marker = dane.get(i)[m];
                if(!frequentItems.contains(marker))
                {
                    for(int j = 0; j < size ; j++)
                    {
                        if(dane.get(j)[m] == marker)
                        {
                            counter++;
                        }
                    }
                    rate = counter/size;
                    //System.out.println(Double.toString(rate));
                    if(rate >= min_sup)
                    {
                        frequentItems.add(marker);
                        itemFrequency.add(counter);
                        
                    }
                }
            }
        }
        if(frequentItems.isEmpty())
        {
            System.out.println("Nie znaleziono zależności");
        }
        else
        {
            System.out.println("Znaleziono następujące zależności: ");
            for(int i = 0;i< frequentItems.size();i++)
            {
                System.out.println(frequentItems.get(i) + " - Występuje " + itemFrequency.get(i).toString() + " razy");
            }
        }
    }

    @Override
    public String toString() {
        return "Office{" +
                "mail='" + mail + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public String[] toString_() {
        return new String[] {getMail(), getCity()};
    }
    
    @Override
    public boolean equals(Object obj) {
        return mail == ((Office) obj).getMail();
    }
    
    private void addTravel(Travel travel) {
        if(!travels.contains(travel)) {
            travels.add(travel);
        }
    }
    
    private boolean equalDirection(Direction d1, Direction d2)
    {
        if(d1.equals(d2))
        {
            return true;
        }
        else
            return false;
    }
}