package sub_buisness_tier.entities;

import java.util.List;
import sub_buisness_tier.TFacade;
import sub_buisness_tier.TFactory;
import java.util.Random;

/**
 * Created by jm on 26.04.17.
 */
public class Main {
    public static void main(String args[]) {
        TFacade facade = new TFacade();
        TFactory factory = new TFactory();
        
        //DANE
        String dane_user1[] = { "Username1","Password1" };
        String dane_user2[] = { "Username2","Password2" };
        String dane_user3[] = { "Username3","Password3" };
        String dane_direction1[] = {"Country1"};
        String dane_direction2[] = {"Country2"};
        String dane_direction3[] = {"Country3"};
        String dane_hotel1[] = { "Hotel1", "Street1","Postal1","City1","Standard1" };
        String dane_hotel2[] = { "Hotel2", "Street2","Postal2","City1","Standard2" };
        String dane_hotel3[] = { "Hotel3", "Street3","Postal3","City2","Standard2" };
        String dane_office1[] = {"Street1","Mail1"};
        String dane_office2[] = {"Street2","Mail2"};
        String dane_office3[] = {"Street3","Mail3"};
        String dane_airport1[] = {"Country1","City1","Code1"};
        String dane_airport2[] = {"Country2","City2","Code2"};
        String dane_airport3[] = {"Country2","City3","Code3"};
        String dane_alimentation1[] = {"Meals1","10"};
        String dane_alimentation2[] = {"Meals2","20"};
        String dane_alimentation3[] = {"Meals3","30"};
        String dane_sport1[] = {"Type1","10"};
        String dane_sport2[] = {"Type2","20"};
        String dane_sport3[] = {"Type3","30"};
        String dane_type1 = "Vacation";
        String dane_type2 = "Tour";
        String dane_type3 = "Stay";
        String dane_period1[] = {"7","20-12-2017","27-12-2017"};
        String dane_period2[] = {"6","20-12-2017","27-12-2017"};
        String dane_period3[] = {"10","20-12-2017","27-12-2017"};
        
        //Tworzenie eleemntów
        Direction d1 = factory.createDirection(dane_direction1);
        Direction d2 = factory.createDirection(dane_direction2);
        Direction d3 = factory.createDirection(dane_direction3);
        Office o1 = factory.createOffice(dane_office1);
        Office o2 = factory.createOffice(dane_office2);
        Office o3 = factory.createOffice(dane_office3);
        TUser u1 = factory.create_user(dane_user1);
        TUser u2 = factory.create_user(dane_user2);
        TUser u3 = factory.create_user(dane_user3);
        Period p1 = factory.createPeriod(dane_period1);
        Period p2 = factory.createPeriod(dane_period2);
        Period p3 = factory.createPeriod(dane_period3);
        Hotel h1 = factory.createHotel(dane_hotel1);
        Hotel h2 = factory.createHotel(dane_hotel2);
        Hotel h3 = factory.createHotel(dane_hotel3);
        AirPort ap1 = factory.create_airport(dane_airport1);
        AirPort ap2 = factory.create_airport(dane_airport2);
        AirPort ap3 = factory.create_airport(dane_airport3);
        Alimentation a1 = factory.createAlimentation(dane_alimentation1);
        Alimentation a2 = factory.createAlimentation(dane_alimentation2);
        Alimentation a3 = factory.createAlimentation(dane_alimentation3);
        SportEntertainment s1 = factory.createSportEntertainment(dane_sport1);
        SportEntertainment s2 = factory.createSportEntertainment(dane_sport2);
        SportEntertainment s3 = factory.createSportEntertainment(dane_sport3);
        
        //Tworzenie podróży
        Travel t1 = new Travel(o1,u1,p1,d1,h1,ap1,a1,s1,dane_type1);
        Travel t2 = new Travel(o1,u1,p2,d1,h2,ap2,a1,s1,dane_type1);
        Travel t3 = new Travel(o1,u2,p2,d1,h2,ap1,a1,s2,dane_type1);
        Travel t4 = new Travel(o1,u2,p1,d3,h1,ap1,a1,s1,dane_type1);
        
        //DODAWANIE
        facade.add_user(dane_user1);
        facade.add_user(dane_user2);
        facade.add_user(dane_user3);
        facade.addDirection(dane_direction1);
        facade.addDirection(dane_direction2);
        facade.addDirection(dane_direction3);
        facade.addOffice(dane_office1);
        facade.addOffice(dane_office2);
        facade.addOffice(dane_office3);
        facade.addAirport(dane_airport1, dane_direction1);
        facade.addAirport(dane_airport2, dane_direction2);
        facade.addAirport(dane_airport3, dane_direction3);
        facade.addHotel(dane_hotel1, dane_direction1);
        facade.addHotel(dane_hotel2, dane_direction2);
        facade.addHotel(dane_hotel3, dane_direction3);
        facade.addAlimentation(dane_alimentation1, dane_direction1, dane_hotel1);
        facade.addAlimentation(dane_alimentation2, dane_direction1, dane_hotel1);
        facade.addAlimentation(dane_alimentation3, dane_direction1, dane_hotel1);
        facade.addAlimentation(dane_alimentation1, dane_direction2, dane_hotel2);
        facade.addAlimentation(dane_alimentation2, dane_direction2, dane_hotel2);
        facade.addAlimentation(dane_alimentation2, dane_direction3, dane_hotel3);
        facade.addAlimentation(dane_alimentation3, dane_direction3, dane_hotel3);
        facade.addSportEntertainment(dane_sport1, dane_direction1, dane_hotel1);
        facade.addSportEntertainment(dane_sport2, dane_direction1, dane_hotel1);
        facade.addSportEntertainment(dane_sport2, dane_direction2, dane_hotel2);
        facade.addSportEntertainment(dane_sport3, dane_direction2, dane_hotel2);
        facade.addSportEntertainment(dane_sport3, dane_direction3, dane_hotel3);
        facade.addSportEntertainment(dane_sport1, dane_direction3, dane_hotel3);
        
        //Wywołanie
        //facade.searchOffice(dane_office1).travels.add(t1);
        //facade.searchOffice(dane_office1).travels.add(t2);
        //facade.searchOffice(dane_office1).travels.add(t3);
        //facade.searchOffice(dane_office1).travels.add(t4);
        
        //petla
        int add = 20;
        Random generator = new Random();
        for(int i=0;i<add;i++)
        {
            int rand;
            Direction d = new Direction();
            TUser u = new TUser();
            Period p = new Period();
            Hotel h = new Hotel();
            AirPort ap = new AirPort();
            Alimentation al = new Alimentation();
            SportEntertainment s = new SportEntertainment();
            Office o = new Office();
            Travel t;
            //Direction
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                d = d1;
            }
            if(rand == 2)
            {
                d = d2;
            }
            if(rand == 3)
            {
                d = d3;
            }
            //User
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                u = u1;
            }
            if(rand == 2)
            {
                u = u2;
            }
            if(rand == 3)
            {
                u = u3;
            }
            //Period
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                p = p1;
            }
            if(rand == 2)
            {
                p = p2;
            }
            if(rand == 3)
            {
                p = p3;
            }
            //Hotel
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                h = h1;
            }
            if(rand == 2)
            {
                h = h2;
            }
            if(rand == 3)
            {
                h = h3;
            }
            //AirPort
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                ap = ap1;
            }
            if(rand == 2)
            {
                ap = ap2;
            }
            if(rand == 3)
            {
                ap = ap3;
            }
            //Alimentation
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                al = a1;
            }
            if(rand == 2)
            {
                al = a2;
            }
            if(rand == 3)
            {
                al = a3;
            }
            //Sport
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                s = s1;
            }
            if(rand == 2)
            {
                s = s2;
            }
            if(rand == 3)
            {
                s = s3;
            }
            //Office + create + add
            rand = generator.nextInt(2)+1;
            if(rand == 1)
            {
                o = o1;
                t = new Travel(o,u,p,d,h,ap,al,s,dane_type1);
                facade.searchOffice(dane_office1).travels.add(t);
            }
            if(rand == 2)
            {
                o = o2;
                t = new Travel(o,u,p,d,h,ap,al,s,dane_type1);
                facade.searchOffice(dane_office2).travels.add(t);
            }
            if(rand == 3)
            {
                o = o3;
                t = new Travel(o,u,p,d,h,ap,al,s,dane_type1);
                facade.searchOffice(dane_office3).travels.add(t3);
            }
        }
        facade.Apriori(dane_office1, dane_direction1, 0.5);
        
    }

}