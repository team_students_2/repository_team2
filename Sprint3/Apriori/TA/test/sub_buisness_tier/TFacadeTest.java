/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collection;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import sub_buisness_tier.entities.Direction;
import sub_buisness_tier.entities.Office;
import sub_buisness_tier.entities.TUser;
import sub_buisness_tier.entities.AirPort;
import sub_buisness_tier.entities.Alimentation;
import sub_buisness_tier.entities.Hotel;
import sub_buisness_tier.entities.SportEntertainment;

@Category({Test_Control.class, Test_Entity.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

/**
 *
 * @author Marcin
 */
public class TFacadeTest {
    static Dane dane;
    static TFacade instance;
    
    public TFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        dane = new Dane();
        instance = new TFacade();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of search_user method, of class TFacade.
     */
    @Test
    public void testSearch_user() {
        System.out.println("Test: search_user");
        TUser user = dane.klient(dane.dane_user1);
        instance = new TFacade();
        instance.add_user(dane.dane_user1);
        TUser expResult = dane.klient(dane.dane_user1);
        TUser result = instance.search_user(dane.dane_user1);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of add_user method, of class TFacade.
     */
    @Test
    public void testAdd_user() {
        System.out.println("add_user");
        String[] data = dane.dane_user1;
        TFacade instance = new TFacade();
        instance.add_user(data);
        TUser expResult = dane.klient(dane.dane_user1);
        TUser result = instance.mUser.get(0);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addOffice method, of class TFacade.
     */
    @Test
    public void testAddOffice() {
        System.out.println("addOffice");
        String[] data = dane.dane_office1;
        TFacade instance = new TFacade();
        instance.addOffice(data);
        Office expResult = dane.biuro(data);
        Office result = instance.officeList.get(0);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of searchOffice method, of class TFacade.
     */
    @Test
    public void testSearchOffice() {
        System.out.println("searchOffice");
        Office office = dane.biuro(dane.dane_office1);
        TFacade instance = new TFacade();
        instance.addOffice(dane.dane_office1);
        Office expResult = dane.biuro(dane.dane_office1);
        Office result = instance.searchOffice(dane.dane_office1);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addDirection method, of class TFacade.
     */
    @Test
    public void testAddDirection() {
        System.out.println("addDirection");
        String[] data = dane.dane_direction1;
        TFacade instance = new TFacade();
        instance.addDirection(dane.dane_direction1);
        Direction expResult = dane.kierunek(dane.dane_direction1);
        Direction result = instance.directionList.get(0);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of searchDirection method, of class TFacade.
     */
    @Test
    public void testSearchDirection() {
        System.out.println("searchDirection");
        Direction direction = dane.kierunek(dane.dane_direction1);
        TFacade instance = new TFacade();
        instance.addDirection(dane.dane_direction1);
        Direction expResult = dane.kierunek(dane.dane_direction1);
        Direction result = instance.searchDirection(dane.dane_direction1);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addAirport method, of class TFacade.
     */
    @Test
    public void testAddAirport() {
        System.out.println("addAirport");
        String[] data = dane.dane_airport1;
        String[] directiondata = dane.dane_direction1;
        TFacade instance = new TFacade();
        instance.addDirection(directiondata);
        instance.addAirport(data, directiondata);
        AirPort expResult = dane.lotnisko(data);
        AirPort result = instance.directionList.get(0).getAirPort();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addAlimentation method, of class TFacade.
     */
    @Test
    public void testAddAlimentation() {
        System.out.println("addAlimentation");
        String[] data = dane.dane_alimentation1;
        String[] directiondata = dane.dane_direction1;
        String[] hoteldata = dane.dane_hotel1;
        Alimentation a = dane.udogodnienie(data);
        TFacade instance = new TFacade();
        instance.addDirection(directiondata);
        instance.addHotel(hoteldata, directiondata);
        instance.addAlimentation(data,directiondata,hoteldata);
        Hotel h = dane.hotel(hoteldata);
        Alimentation expResult = dane.udogodnienie(data);
        Alimentation result = instance.directionList.get(0).searchHotel(hoteldata).searchAlimentation(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addHotel method, of class TFacade.
     */
    @Test
    public void testAddHotel() {
        System.out.println("addHotel");
        String[] data = dane.dane_hotel1;
        String[] directiondata = dane.dane_direction1;
        TFacade instance = new TFacade();
        instance.addDirection(dane.dane_direction1);
        instance.addHotel(data,dane.dane_direction1);
        Hotel expResult = dane.hotel(data);
        Hotel h = dane.hotel(data);
        Hotel result = instance.directionList.get(0).searchHotel(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of addSportEntertainment method, of class TFacade.
     */
    @Test
    public void testAddSportEntertainment() {
        System.out.println("addSportEntertainment");
        String[] data = dane.dane_sport1;
        String[] directiondata = dane.dane_direction1;
        String[] hoteldata = dane.dane_hotel1;
        SportEntertainment s = dane.sport(data);
        TFacade instance = new TFacade();
        instance.addDirection(directiondata);
        instance.addHotel(hoteldata, directiondata);
        instance.addSportEntertainment(data,directiondata,hoteldata);
        Hotel h = dane.hotel(dane.dane_hotel1);
        SportEntertainment expResult = dane.sport(data);
        SportEntertainment result = instance.directionList.get(0).searchHotel(hoteldata).searchSportEntertainment(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testGetOffices() {
        System.out.println("getOffices");
        TFacade instance = new TFacade();
        instance.addOffice(dane.dane_office1);
        Office o = dane.biuro(dane.dane_office1);
        List<Office> lista1 = new ArrayList<>();
        lista1.add(o);
        List<Office> expResult = lista1;
        List<Office> result = instance.getOffices();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setOffices method, of class TFacade.
     */
    @Test
    public void testSetOffices() {
        System.out.println("setOffices");
        List<Office> lista1 = new ArrayList<>();
        Office o = dane.biuro(dane.dane_office1);
        lista1.add(o);
        TFacade instance = new TFacade();
        instance.setOffices(lista1);
        List<Office> expResult = lista1;
        List<Office> result = instance.getOffices();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDirectionList method, of class TFacade.
     */
    @Test
    public void testGetDirectionList() {
        System.out.println("getDirectionList");
        TFacade instance = new TFacade();
        instance.addDirection(dane.dane_direction1);
        List<Direction> lista1 = new ArrayList<>();
        Direction d = dane.kierunek(dane.dane_direction1);
        lista1.add(d);
        List<Direction> expResult = lista1;
        List<Direction> result = instance.getDirectionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDirectionList method, of class TFacade.
     */
    @Test
    public void testSetDirectionList() {
        System.out.println("setDirectionList");
        List<Direction> lista1 = new ArrayList<>();
        Direction dir = dane.kierunek(dane.dane_direction1);
        lista1.add(dir);
        TFacade instance = new TFacade();
        instance.addDirection(dane.dane_direction1);
        instance.setDirectionList(lista1);
        List<Direction> expResult = lista1;
        List<Direction> result = instance.getDirectionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setmUser method, of class TFacade.
     */
    @Test
    public void testSetmUser() {
        System.out.println("setmUser");
        List<TUser> lista1 = new ArrayList<>();
        TUser u = dane.klient(dane.dane_user1);
        lista1.add(u);
        TFacade instance = new TFacade();
        instance.setmUser(lista1);
        List<TUser> expResult = lista1;
        List<TUser> result = instance.getmUser();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getmUser method, of class TFacade.
     */
    @Test
    public void testGetmUser() {
        System.out.println("getmUser");
        List<TUser> lista1 = new ArrayList<>();
        TUser u = dane.klient(dane.dane_user1);
        lista1.add(u);
        TFacade instance = new TFacade();
        instance.add_user(dane.dane_user1);
        List<TUser> expResult = lista1;
        List<TUser> result = instance.getmUser();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    
    //Głębsze testy? Mock?
    /**
     * Test of getDirectionDataList method, of class TFacade.
     */
    @Test
    public void testGetDirectionDataList() {
        //Błąd
        //Tworzony jest nowy obiekt ArrayList<String[]>() w metodzie getDirectionDataList
        //Przez to nie można porównać obiektów
        System.out.println("getDirectionDataList");
        TFacade instance = new TFacade();
        instance.addDirection(dane.dane_direction1);
        List<String[]> lista1 = new ArrayList<String[]>();
        Direction dir = dane.kierunek(dane.dane_direction1);
        lista1.add(dir.getData());
        List <String[]> lista2 = instance.getDirectionDataList();
        boolean expResult = true;
        boolean result = true;
        //List expResult = lista1;
        //List result = instance.getDirectionDataList();
        for(int i=0;i<lista1.size();i++)
        {
            //System.out.println(lista1.get(i)[0]);
            //System.out.println(lista2.get(i)[0]);
            if(lista1.get(i)[0] != instance.getDirectionDataList().get(i)[0])
            {
                result = false;
            }
        }
        //Sprawdzaj zawartość - inne niż assertEquals
        //Run with Parametrized Class?
        assertEquals(expResult, result);
    }
}
