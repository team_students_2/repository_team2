package sub_buisness_tier.entities;

public class Travel {
    
    private Office m_office;
    private TUser m_user;
    private Period m_period;
    private Direction m_direction;
    private Hotel m_hotel;
    private AirPort m_airport;
    private Alimentation alimentation;
    private SportEntertainment sport;
    private String typ_wyjazduData[];

    public Travel(Office m_office, TUser m_user, Period m_period, Direction m_direction, Hotel m_hotel, AirPort m_airport, Alimentation alimentation, SportEntertainment sport, String[] typ_wyjazduData) {
        this.m_office = m_office;
        this.m_user = m_user;
        this.m_period = m_period;
        this.m_direction = m_direction;
        this.m_hotel = m_hotel;
        this.m_airport = m_airport;
        this.alimentation = alimentation;
        this.sport = sport;
        this.typ_wyjazduData = typ_wyjazduData;
        System.out.println("Travel add");
    }
}
