package sub_buisness_tier.entities;

import java.util.Date;


public class TFlight {
    
    private String m_start;
    private String m_end;
    private int m_num_seats;
    private int m_num_remaining_seats;
    
    public TFlight(String start, String end, int num_seats)
    {
        m_start = start;
        m_end = end;
        m_num_seats = num_seats;
        m_num_remaining_seats = num_seats;
    }

    public String getStart() {
        return m_start;
    }

    public void setStart(String m_start) {
        this.m_start = m_start;
    }

    public String getEnd() {
        return m_end;
    }

    public void setEnd(String m_end) {
        this.m_end = m_end;
    }
    
    public void occupySeat()
    {
        if(m_num_remaining_seats > 0)
            m_num_remaining_seats--;
    }
    
    public int getNumRemainingSeats()
    {
        return m_num_remaining_seats;
    }
    
    
}
