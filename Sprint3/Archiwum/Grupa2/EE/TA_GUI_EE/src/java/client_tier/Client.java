package client_tier;

import javax.ejb.EJB;
//import sub_buisness_tier.TFacade;
import sub_buisness_tier_ejb.Facade_sub_buisness_tier_ejbRemote;
import travel.agency.Panel_util;

/**
 *
 * @author Damian
 */
public class Client {

    @EJB
    private static Facade_sub_buisness_tier_ejbRemote facade;
    
    
    //static TFacade facade = new TFacade();

    static public Facade_sub_buisness_tier_ejbRemote getFacade() {
        return facade; }

    static public void setFacade(Facade_sub_buisness_tier_ejbRemote facade) {
        Client.facade = facade; }
    
    public static void main(String[] args) {
       java.awt.EventQueue.invokeLater(() -> {
           Panel_util.createAndShowGUI();
       });
    } 
}
