package sub_buisness_tier;

/**
 * Created by jm on 19.04.17.
 */
import java.util.ArrayList;
import java.util.List;
import sub_buisness_tier.entities.Direction;
import sub_buisness_tier.entities.Office;
import sub_buisness_tier.entities.TUser;

public class TFacade {

    List<Office> officeList;
    List<TUser> mUser;
    List<Direction> directionList;

    public TFacade() {
        officeList = new ArrayList();
        mUser = new ArrayList<>();
        directionList = new ArrayList();
        Direction direction = new Direction();
        direction.setCountry("test1");
        direction.addHotel(new String[] {"test1", "test1", "test1", "test1", "test1"});
        direction.addAirport(new String[] {"test1", "test1", "test1"});
        directionList.add(direction);
        direction = new Direction();
        direction.setCountry("test2");
        direction.addHotel(new String[] {"test2", "test2", "test2", "test2", "test2"});
        direction.addAirport(new String[] {"test2", "test2", "test2"});
        directionList.add(direction);
        addOffice(new String[] {"test1", "test1"});
        addOffice(new String[] {"test2", "test2"});
        add_user(new String[] {"test1", "test1"});
        add_user(new String[] {"test2", "test2"});
    }

    public void clientOffer(String userData[], String directionData[], String hotelData[], String alimentationData[], String sportData[], String typ_wyjazduData[], String periodData[], String officeData[]) {
        TFactory factory = new TFactory();
        TUser user = factory.create_user(userData);
        user = search_user(user);
        if (user != null) {
            System.out.println("OK1");
            Office office = factory.createOffice(officeData);
            office = searchOffice(office);
            if (office != null) {
                System.out.println("OK2");
                Direction direction = factory.createDirection(directionData);
                direction = searchDirection(direction);
                if (direction != null) {
                    System.out.println("OK3");
                    Object hotelAlimentationSport[] = direction.checkHotel(hotelData, alimentationData, sportData, periodData);
                    Object airportPeriod[] = direction.checkAirport(periodData);
                    System.out.println("OK4");
                    if (hotelAlimentationSport != null && airportPeriod != null) {
                        System.out.println("OK5");
                        office.createTravel(user, direction, hotelAlimentationSport, airportPeriod, typ_wyjazduData);
                    }
                }
            }
        }
    }

//USER HANDLING
    public TUser search_user(TUser user) {
        int idx;
        if ((idx = mUser.indexOf(user)) != -1) {
            user = mUser.get(idx);
            return user;
        }
        return null;
    }

    public String add_user(String data[]) {
        TFactory factory = new TFactory();
        TUser user = factory.create_user(data);

        if (search_user(user) == null) {
            mUser.add(user);
            System.out.println("Added");
            return user.toString();
        }
        return null;
    }

    public void setmUser(List<TUser> mUser) {

        this.mUser = mUser;
    }

    public List<TUser> getmUser() {

        return mUser;
    }

    public String addOffice(String data[]) {
        TFactory factory = new TFactory();
        Office office = factory.createOffice(data);
        if ((searchOffice(office)) == null) {
            officeList.add(office);
            return office.toString();
        }
        return null;
    }

    public Office searchOffice(Office office) {
        int idx = officeList.indexOf(office);
        if (idx != -1) {
            return officeList.get(idx);
        }
        return null;
    }
    //DIRECTION HANDLING

    public String addDirection(String data[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(data);
        if ((searchDirection(direction)) == null) {
            directionList.add(direction);
            return direction.toString();
        }
        return null;
    }

    public Direction searchDirection(Direction direction) {
        int idx = directionList.indexOf(direction);
        if (idx != -1) {
            return directionList.get(idx);
        }
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

    public Object[][] getOffice() {
        Object[][] office = new Object[officeList.size()][];
        int i = 0;
        for (Office next : officeList) {
            office[i++] = next.toString_();
        }
        return office;
    }

    public Object[][] getDirection() {
        Object[][] direction = new Object[directionList.size()][];
        int i = 0;
        for (Direction next : directionList) {
            direction[i++] = next.toString_();
        }
        return direction;
    }

    public Object[][] getHotel(String directionData[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directionData);
        direction = searchDirection(direction);
        if (direction != null) {
            return direction.getHotel();
        }
        return null;
    }
    
    public Object[][] getAlimentation(String directionData[], String hotelData[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directionData);
        direction = searchDirection(direction);
        if (direction != null) {
            return direction.getAlimentation(hotelData);
        }
        return null;
    }
    
    public Object[][] getSport(String directionData[], String hotelData[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directionData);
        direction = searchDirection(direction);
        if (direction != null) {
            return direction.getSport(hotelData);
        }
        return null;
    }
}
