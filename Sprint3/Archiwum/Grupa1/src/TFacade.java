/**
 * Created by jm on 19.04.17.
 */
import java.util.ArrayList;
import java.util.List;
public class TFacade {

    List<Office> officeList;
    List<Direction> directionList;
    List<TUser> mUser;

    public TFacade() {

        officeList = new ArrayList<>();
        directionList = new ArrayList<>();
        mUser = new ArrayList<>();
        //periodList = new ArrayList<>();
    }
/////////////////////////////////////////////////////////////////////////////////////////

//USER HANDLING
    public TUser search_user (TUser user) {
        int idx;
        if ((idx = mUser.indexOf(user)) != -1) {
            user = mUser.get(idx);
            return user;
        }
        return null;
    }

    public String add_user(String data[]) {
        TFactory factory = new TFactory();
        TUser user = factory.create_user(data);

        if (search_user(user) == null) {
            mUser.add(user);
            System.out.println("Added");
            return user.toString();
        }
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////


//OFFICE HANDLING

    public String addOffice(String data[]) {
        TFactory factory = new TFactory();
        Office office = factory.createOffice(data);
        if ((searchOffice(office)) == null) {
            officeList.add(office);
            return office.toString();
        }
        return null;
    }

    public Office searchOffice(Office office) {
        int idx = officeList.indexOf(office);
        if(idx!=-1)
            return officeList.get(idx);
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

//DIRECTION HANDLING
    public String addDirection(String data[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(data);
        if ((searchDirection(direction)) == null) {
            directionList.add(direction);
            return direction.toString();
        }
        return null;
    }

    public Direction searchDirection(Direction direction) {
        int idx = directionList.indexOf(direction);
        if(idx!=-1)
            return directionList.get(idx);
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT ADDING
    public String addAirport(String data[], Direction direction){
        direction = searchDirection(direction);
        if (direction != null){
            direction.addAirport(data);
        }

        return null;
    }


//ALIMENTATION ADDING
    public String addAlimentation(String data[], Direction direction, Hotel hotel) {

        direction = searchDirection(direction);
        if (direction != null){
            direction.addAlimentation(data, hotel);
        }

        return null;
    }


//HOTEL ADDING
    public String addHotel(String data[], Direction direction){
        direction = searchDirection(direction);
        if (direction != null){
            direction.addHotel(data);
        }
        else
            System.out.println("Nie ma takiego kierunku");

        return null;
    }

//SPORT AND ENTERTAINMENT ADDING
    public String addSportEntertainment(String data[], Direction direction, Hotel hotel){
        direction = searchDirection(direction);
        if (direction != null){
            direction.addSportEntertainment(data, hotel);
        }

        return null;
    }
///////////////////////////////////////////////////////////////////////////


//JUST FOR HELP
    public void printOffices() {
        for (Office off : officeList) {
            System.out.println(off.toString());
        }

    }


    public void printDirection() {
        for (Direction dir : directionList) {
            System.out.println(dir.toString());
        }

    }

//GETTERS AND SETTERS
    public List<Office> getOffices() {

        return officeList;
    }

    void setOffices(List<Office> officeList) {

        this.officeList = officeList;
    }


    public List<Direction> getDirectionList() {

        return directionList;
    }

    public void setDirectionList(List<Direction> directionList) {

        this.directionList = directionList;
    }


    public void setmUser(List<TUser> mUser) {

        this.mUser = mUser;
    }

    public List<TUser> getmUser() {

        return mUser;
    }


    }

