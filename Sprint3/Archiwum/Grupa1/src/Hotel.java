


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

    public class Hotel {

        private String name;
        private String street;
        private String postalCode;
        private String city;
        private String standard;

        private List<Alimentation> alimentationList;
        private List<HotelVacancies> hotelVacanciesList;
        private Direction direction;
        private List<SportEntertainment> sportEntertainmentList;


        public Hotel(){
            alimentationList = new ArrayList<>();
            hotelVacanciesList = new ArrayList<>();
            sportEntertainmentList = new ArrayList<>();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getStandard() {
            return standard;
        }

        public void setStandard(String standard) {
            this.standard = standard;
        }



        public void addAlimentation(String data []) {
            TFactory factory = new TFactory();
            Alimentation alimentation = factory.createAlimentation(data);
            if ((searchAlimentation(alimentation)) == null) {
                alimentationList.add(alimentation);
                System.out.println("Dodano wyzywienie" + alimentation);

            }
        }

        public Alimentation searchAlimentation (Alimentation alimentation) {
            int idx = alimentationList.indexOf(alimentation);
            if(idx!=-1)
                return alimentationList.get(idx);
            return null;

        }

        public SportEntertainment searchSportEntertainment (SportEntertainment sport) {
            int idx = sportEntertainmentList.indexOf(sport);
            if(idx!=-1)
                return sportEntertainmentList.get(idx);
            return null;
        }

        public void addSportEntertainment(String data[]){
            TFactory factory = new TFactory();
            SportEntertainment sport = factory.createSportEntertainment(data);
            if ((searchSportEntertainment(sport)) == null) {
                sportEntertainmentList.add(sport);
                System.out.println("Dodano sport" + sport);

            }
        }


        @Override
        public String toString() {
            return "Hotel{" + "name=" + name + ", street=" + street + ", postalCode=" + postalCode + ", city=" + city + ", standard=" + standard + '}';
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 37 * hash + Objects.hashCode(this.name);
            hash = 37 * hash + Objects.hashCode(this.street);
            hash = 37 * hash + Objects.hashCode(this.postalCode);
            hash = 37 * hash + Objects.hashCode(this.city);
            hash = 37 * hash + Objects.hashCode(this.standard);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Hotel other = (Hotel) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            if (!Objects.equals(this.street, other.street)) {
                return false;
            }
            if (!Objects.equals(this.postalCode, other.postalCode)) {
                return false;
            }
            if (!Objects.equals(this.city, other.city)) {
                return false;
            }
            if (!Objects.equals(this.standard, other.standard)) {
                return false;
            }
            return true;
        }
    }

