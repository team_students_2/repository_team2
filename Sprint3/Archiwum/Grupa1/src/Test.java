import org.junit.Test;
import static org.junit.Assert.*;

public class OfficeTest {
    

    /**
     * Test of setMail and getMail methods, of class Office.
     */
    @Test
    public void testGetSetMail() {
        System.out.println("setMail");
        String mail = "abcd@mail.com";
        Office instance = new Office();
        instance.setMail(mail);
        
        assertEquals(instance.getMail(), mail);
    }

    /**
     * Test of setCity and getCity methods, of class Office.
     */
    @Test
    public void testGetSetCity() {
        System.out.println("setCity");
        String city = "Warsaw";
        Office instance = new Office();
        instance.setCity(city);
        
        assertEquals(instance.getCity(), city);
    }
    
    @Test
    public void testForm_airport() {
        System.out.println("form_airport");
        Airport_form instance = new Airport_form();
        instance.country.setText("CountryName");
        instance.city.setText("CityName");
        instance.code.setText("Code");
        String[] expResult = {"CountryName","CityName","Code"};
        String[] result = instance.form_airport();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of content_validate method, of class Airport_form.
     */
    @Test
    public void testContent_validate() {
        System.out.println("content_validate");
        JTextField val = new JTextField("Testing Content Validate");
        Airport_form instance = new Airport_form();
        String expResult = "Testing_Content_Validate";
        String result = instance.content_validate(val);
        assertEquals(expResult, result);
    }

    /**
     * Test of content_validate method, of class AddUser_form.
     */
    @org.junit.Test
    public void testContent_validate() {
        System.out.println("content_validate");
        JTextField val = new JTextField("Testing Content Validate");
        AddUser_form instance = new AddUser_form();
        String expResult = "Testing_Content_Validate";
        String result = instance.content_validate(val);
        assertEquals(expResult, result);
    }

    /**
     * Test of password_securitycheck method, of class AddUser_form.
     */
    @Theory
    public void testPassword_securitycheck(String expResult, String password) {
        System.out.println("password_securitycheck");
        JPasswordField val = new JPasswordField(password);
        AddUser_form instance = new AddUser_form();
        String result = instance.password_securitycheck(val);
        assertEquals(result, expResult);
    }

    /**
     * Test of getSportList method, of class TFacade.
     */
    @Test
    public void testGetSportList() {
        System.out.println("getSportList");
        Facade instance = new Facade();
        ArrayList<Sport> expResult = null;
        ArrayList<Sport> result = instance.getSportList();
        assertEquals(expResult, result);
        
    }

    

    /**
     * Test of addSport method, of class TFacade.
     */
    @Test
    public void testAddSport() {
        System.out.println("addSport");
        String[] data = {"Sport","Cena"};
        Facade instance = new Facade();
        String[] expResult = {"Sport","Cena"};
        String result = instance.addSport(data);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of searchSport method, of class TFacade.
     */
    @Test
    public void testSearchSport() {
        System.out.println("searchSport");
        Sport sport = null;
        Facade instance = new Facade();
        Sport expResult = null;
        Sport result = instance.searchSport(sport);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getAmenityList method, of class TFacade.
     */
    @Test
    public void testGetAmenityList() {
        System.out.println("getAmenityList");
        Facade instance = new Facade();
        ArrayList<Amenity> expResult = null;
        ArrayList<Amenity> result = instance.getAmenityList();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setAmenityList method, of class TFacade.
     */
    @Test
    public void testSetAmenityList() {
        System.out.println("setAmenityList");
        ArrayList<Amenity> amenityList = null;
        Facade instance = new Facade();
        instance.setAmenityList(amenityList);
        
    }

    /**
     * Test of addAmenity method, of class TFacade.
     */
    @Test
    public void testAddAmenity() {
        System.out.println("addAmenity");
        String[] data = null;
        Facade instance = new Facade();
        String expResult = "";
        String result = instance.addAmenity(data);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of searchAmenity method, of class TFacade.
     */
    @Test
    public void testSearchAmenity() {
        System.out.println("searchAmenity");
        Amenity amenity = null;
        Facade instance = new Facade();
        Amenity expResult = null;
        Amenity result = instance.searchAmenity(amenity);
        assertEquals(expResult, result);
        
    }
}