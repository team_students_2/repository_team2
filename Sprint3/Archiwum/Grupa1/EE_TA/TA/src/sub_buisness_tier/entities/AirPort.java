package sub_buisness_tier.entities;

public class AirPort {

    private String country;
    private String city;
    private String code;
    private Direction flightDirection;


    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getCode() {
        return code;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {

        String fullName = getCity() + ", " + getCountry() + " - " + getCode();

        return fullName;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null){
            return false;
        }
        if(!AirPort.class.isAssignableFrom(obj.getClass())){
            return false;
        }
        final AirPort other = (AirPort) obj;
        if((this.code == null) ? (other.code != null) : !this.code.equals(other.code)){
            return false;
        }
        return true;
    }

}