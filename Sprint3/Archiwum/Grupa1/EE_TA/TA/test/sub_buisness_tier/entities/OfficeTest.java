/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier.entities;

import org.junit.Test;
import static org.junit.Assert.*;


public class OfficeTest {
    
    public OfficeTest() {
    }

    /**
     * Test of setMail method, of class Office.
     */
    @Test
    public void testSetMail() {
        System.out.println("setMail");
        String mail = "test";
        Office instance = new Office();
        instance.setMail(mail);
        assertEquals(mail, instance.getMail());
    }

    /**
     * Test of getMail method, of class Office.
     */
    @Test
    public void testGetMail() {
        System.out.println("getMail");
        Office instance = new Office();
        String expResult = "Test123@gmail.com";
        instance.setMail("Test123@gmail.com");
        String result = instance.getMail();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCity method, of class Office.
     */
    @Test
    public void testSetCity() {
        System.out.println("setCity");
        String city = "Wrocław";
        Office instance = new Office();
        instance.setCity(city);
        assertEquals(city, instance.getCity());
    }

    /**
     * Test of getCity method, of class Office.
     */
    @Test
    public void testGetCity() {
        System.out.println("getCity");
        Office instance = new Office();
        String expResult = "Wrocław";
        instance.setCity("Wroclaw");
        String result = instance.getCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Office.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Office instance = new Office();
        instance.setMail("Test123@gmail.com");
        instance.setCity("Wrocław");
        String expResult = "Office{" +
                "mail='" + "Test123@gmail.com" + '\'' +
                ", city='" + "Wrocław" + '\'' +
                '}';
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
