package travel.agency;

import client_tier.Client;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import sub_buisness_tier.entities.Direction;

/**
 *
 * @author LukaszLech
 */
public class Direction_form extends JPanel implements ActionListener{
    Direction direction = new Direction();
    
    JLabel lcountry = new JLabel("Country");
    JTextField country = new JTextField(30);
    JLabel lairport = new JLabel("Airport");
    JTextField airport = new JTextField(30);
    JButton add_direction = new JButton("Add Direction");

    public Direction_form() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(lcountry);
        add(country);
        add(lairport);
        add(airport);
        add(add_direction);
       
    }
    
    public void init() {
        add_direction.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] data = form_addDirection();
        if (data == null) {
            return;
        }
        Client.getFacade().addDirection(data);
    }

    private String[] form_addDirection() {
        if (content_validate(country) == null) {
            return null;
        } 
        if (content_validate(airport) == null) {
            return null;
        }
        String data[] = {country.getText(), airport.getText()};
        return data;
    }

    private Object content_validate(JTextField val) {
        String s = val.getText();
        if (s.equals("")) {
            JOptionPane.showMessageDialog(this, "required value");
            return null;
        } else {
            s = s.replaceAll(" ", "_");
            val.setText(s);
            return s;
        }
    }
    
}
