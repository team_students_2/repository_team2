/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import sub_buisness_tier.entities.AirPort;
import sub_buisness_tier.entities.Alimentation;
import sub_buisness_tier.entities.Direction;
import sub_buisness_tier.entities.Hotel;
import sub_buisness_tier.entities.Office;
import sub_buisness_tier.entities.SportEntertainment;
import sub_buisness_tier.entities.TUser;

/**
 *
 * @author Marcin
 */

@Category({Test_Control.class, Test_Entity.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class TFactoryTest {
    static Dane dane;
    static TFactory instance;
    
    public TFactoryTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        dane = new Dane();
        instance = new TFactory();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of createOffice method, of class TFactory.
     */
    @Test
    public void testCreateOffice() {
        System.out.println("createOffice");
        String[] data = dane.dane_office1;
        TFactory instance = new TFactory();
        Office expResult = dane.biuro(data);
        Office result = instance.createOffice(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createAlimentation method, of class TFactory.
     */
    @Test
    public void testCreateAlimentation() {
        System.out.println("createAlimentation");
        String[] data = dane.dane_alimentation1;
        TFactory instance = new TFactory();
        Alimentation expResult = dane.udogodnienie(data);
        Alimentation result = instance.createAlimentation(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createHotel method, of class TFactory.
     */
    @Test
    public void testCreateHotel() {
        System.out.println("createHotel");
        String[] data = dane.dane_hotel1;
        TFactory instance = new TFactory();
        Hotel expResult = dane.hotel(data);
        Hotel result = instance.createHotel(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createDirection method, of class TFactory.
     */
    @Test
    public void testCreateDirection() {
        System.out.println("createDirection");
        String[] data = dane.dane_direction1;
        TFactory instance = new TFactory();
        Direction expResult = dane.kierunek(data);
        Direction result = instance.createDirection(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createSportEntertainment method, of class TFactory.
     */
    @Test
    public void testCreateSportEntertainment() {
        //FAIL - SportEntertainment extends Convenince?
        System.out.println("createSportEntertainment");
        String[] data = dane.dane_sport1;
        TFactory instance = new TFactory();
        SportEntertainment expResult = dane.sport(data);
        SportEntertainment result = instance.createSportEntertainment(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of create_user method, of class TFactory.
     */
    @Test
    public void testCreate_user() {
        System.out.println("create_user");
        String[] data = dane.dane_user1;
        TFactory instance = new TFactory();
        TUser expResult = dane.klient(data);
        TUser result = instance.create_user(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of create_airport method, of class TFactory.
     */
    @Test
    public void testCreate_airport() {
        System.out.println("create_airport");
        String[] data = dane.dane_airport1;
        TFactory instance = new TFactory();
        AirPort expResult = dane.lotnisko(data);
        AirPort result = instance.create_airport(data);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
