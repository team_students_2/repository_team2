/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business_tier_ejb;

import integration_tier_ejb.AlimentationFacadeLocal;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import sub_buisness_tier.TFacade;
import sub_buisness_tier.entities.Alimentation;

/**
 *
 * @author PWR
 */
@Stateless
public class Facade_ejb implements Facade_ejbRemote {

    public AlimentationFacadeLocal getAlimentationFacade() {
        return alimentationFacade;
    }

    public void setAlimentationFacade(AlimentationFacadeLocal alimentationFacade) {
        this.alimentationFacade = alimentationFacade;
    }

    @EJB
    private AlimentationFacadeLocal alimentationFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    TFacade facade = new TFacade();
    
    @PostConstruct
    public void init(){
        try{
            getFromDatabase();
        } catch(Exception e){}
    }
    
    
    ///////////////
    public void getFromDatabase(){
        List<Alimentation> a = alimentationFacade.findAll();
        
        facade.alimentationsFromDatabase(a);
        
    }
        
    public void addToDatabase(){
        for (Alimentation a : facade.getAlimentationList()){
            Long id = a.getId();
            if(id == null || alimentationFacade.find(a.getId()) == null){
                alimentationFacade.create(a);
            }
        }
    }
    /////////////////
    
    
    public String add_user(String data[]) {
       return facade.add_user(data);
    }

    
    public String add_alimentation(String data[]){
        return facade.add_alimentation(data);
    }

//OFFICE HANDLING

    public String addOffice(String data[]) {
       return facade.addOffice(data);
    }

   


//DIRECTION HANDLING
    public String addDirection(String data[]) {
        return facade.addDirection(data);
    }

  
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT ADDING
    public String addAirport(String data[], String directiondata[]){
      return facade.addAirport(data, directiondata);
    }


//ALIMENTATION ADDING
    public String addAlimentation(String data[], String directiondata[], String hoteldata[]) {
       return facade.addAlimentation(data, directiondata, hoteldata);
    }


//HOTEL ADDING
    public String addHotel(String data[], String directiondata[]){
       return facade.addHotel(data, directiondata);
    }

//SPORT AND ENTERTAINMENT ADDING
    public String addSportEntertainment(String data[], String directiondata[], String hoteldata[]){
       return facade.addSportEntertainment(data, directiondata, hoteldata);
    }

    public List<String[]> getDirectionDataList() {
      return facade.getDirectionDataList();
    }

}
