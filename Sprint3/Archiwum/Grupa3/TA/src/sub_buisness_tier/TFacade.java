package sub_buisness_tier;

/**
 * Created by jm on 19.04.17.
 */
import java.util.ArrayList;
import java.util.List;
import sub_buisness_tier.entities.Direction;
import sub_buisness_tier.entities.Hotel;
import sub_buisness_tier.entities.Office;
import sub_buisness_tier.entities.TUser;
import sub_buisness_tier.TFactory;
import sub_buisness_tier.entities.Alimentation;

public class TFacade {

    List<Office> officeList;
    List<Direction> directionList;
    List<TUser> mUser;
    List<Alimentation> alimentationList;

    public TFacade() {

        officeList = new ArrayList<>();
        directionList = new ArrayList<>();
        mUser = new ArrayList<>();
        alimentationList = new ArrayList<>();
        
        //periodList = new ArrayList<>();
    }
/////////////////////////////////////////////////////////////////////////////////////////

    
    public void alimentationsFromDatabase (List<Alimentation> alimentation_){
            
        alimentationList.clear();
        alimentationList.addAll(alimentation_);
        
    }
    
    public String add_alimentation(String data[]) {
        TFactory factory = new TFactory();
        Alimentation alimentation = factory.createAlimentation(data);
        
        alimentationList.add(alimentation);
        System.out.println("Added");
        
        return null;
    }
    
//USER HANDLING
    public TUser search_user (TUser user) {
        int idx;
        if ((idx = mUser.indexOf(user)) != -1) {
            user = mUser.get(idx);
            return user;
        }
        return null;
    }

    public List<Alimentation> getAlimentationList() {
        return alimentationList;
    }

    public void setAlimentationList(List<Alimentation> alimentationList) {
        this.alimentationList = alimentationList;
    }

    public String add_user(String data[]) {
        TFactory factory = new TFactory();
        TUser user = factory.create_user(data);

        if (search_user(user) == null) {
            mUser.add(user);
            System.out.println("Added");
            return user.toString();
        }
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////


//OFFICE HANDLING

    public String addOffice(String data[]) {
        TFactory factory = new TFactory();
        Office office = factory.createOffice(data);
        if ((searchOffice(office)) == null) {
            officeList.add(office);
            return office.toString();
        }
        return null;
    }

    public Office searchOffice(Office office) {
        int idx = officeList.indexOf(office);
        if(idx!=-1)
            return officeList.get(idx);
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

//DIRECTION HANDLING
    public String addDirection(String data[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(data);
        if ((searchDirection(direction)) == null) {
            directionList.add(direction);
            System.out.println(direction + " added");
            return direction.toString();
        }
        return null;
    }

    public Direction searchDirection(Direction direction) {
        int idx = directionList.indexOf(direction);
        if(idx!=-1)
            return directionList.get(idx);
        return null;
    }
////////////////////////////////////////////////////////////////////////////////////////////

//AIRPORT ADDING
    public String addAirport(String data[], String directiondata[]){
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directiondata);
        direction = searchDirection(direction);
        if (direction != null){
            direction.addAirport(data);
        }

        return null;
    }


//ALIMENTATION ADDING
    public String addAlimentation(String data[], String directiondata[], String hoteldata[]) {
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directiondata);
        direction = searchDirection(direction);
        if (direction != null){
            direction.addAlimentation(data, hoteldata);
        }

        return null;
    }


//HOTEL ADDING
    public String addHotel(String data[], String directiondata[]){
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directiondata);
        direction = searchDirection(direction);
        if (direction != null){
            direction.addHotel(data);
        }
        else
            System.out.println("Nie ma takiego kierunku");

        return null;
    }

//SPORT AND ENTERTAINMENT ADDING
    public String addSportEntertainment(String data[], String directiondata[], String hoteldata[]){
        TFactory factory = new TFactory();
        Direction direction = factory.createDirection(directiondata);
        direction = searchDirection(direction);
        if (direction != null){
            direction.addSportEntertainment(data, hoteldata);
        }

        return null;
    }
///////////////////////////////////////////////////////////////////////////


//JUST FOR HELP
    public void printOffices() {
        for (Office off : officeList) {
            System.out.println(off.toString());
        }

    }


    public void printDirection() {
        for (Direction dir : directionList) {
            System.out.println(dir.toString());
        }

    }

//GETTERS AND SETTERS
    public List<Office> getOffices() {

        return officeList;
    }

    void setOffices(List<Office> officeList) {

        this.officeList = officeList;
    }


    public List<Direction> getDirectionList() {

        return directionList;
    }
    
    /*
        public Direction createDirection(String data[]) {
        Direction direction = null;
        direction = new Direction();
        direction.setCountry(data[0]);

        return direction;
    }
    */
    //TODO : Czy mozna to rozwiazac prosciej? 
    public List<String[]> getDirectionDataList() {
        List<String[]> directiondataList = new ArrayList<String[]>();
        for(Direction direction : directionList)
        {
            directiondataList.add(direction.getData());
        }
        return directiondataList;
    }

    public void setDirectionList(List<Direction> directionList) {

        this.directionList = directionList;
    }


    public void setmUser(List<TUser> mUser) {

        this.mUser = mUser;
    }

    public List<TUser> getmUser() {

        return mUser;
    }


    }

