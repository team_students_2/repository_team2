package sub_buisness_tier.entities;

/**
 * Created by jm on 26.04.17.
 */
public class Convenience {
    protected String type;
    protected int price;

//GETTERS AND SETTERS
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
