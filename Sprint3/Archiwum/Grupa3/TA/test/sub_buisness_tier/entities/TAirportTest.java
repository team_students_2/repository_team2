/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sub_buisness_tier.entities;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.theories.*;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class TAirportTest {
    
    public TAirportTest() {
    }

    /**
     * Test of getCountry method, of class TAirport.
     */
    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        TAirport instance = new TAirport();
        instance.setCountry("Infernus");
        String expResult = "Infernus";
        String result = instance.getCountry();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCity method, of class TAirport.
     */
    @Test
    public void testGetCity() {
        System.out.println("getCity");
        TAirport instance = new TAirport();
        instance.setCity("Infernus");
        String expResult = "Infernus";
        String result = instance.getCity();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCode method, of class TAirport.
     */
    @Test
    public void testGetCode() {
        System.out.println("getCode");
        TAirport instance = new TAirport();
        instance.setCode("Infernus");
        String expResult = "Infernus";
        String result = instance.getCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCountry method, of class TAirport.
     */
    @Test
    public void testSetCountry() {
        System.out.println("setCountry");
        String country = "Infernus";
        TAirport instance = new TAirport();
        instance.setCountry(country);
        assertEquals(country, instance.getCountry());
    }

    /**
     * Test of setCity method, of class TAirport.
     */
    @Test
    public void testSetCity() {
        System.out.println("setCity");
        String city = "Infernus";
        TAirport instance = new TAirport();
        instance.setCity(city);
        assertEquals(city, instance.getCity());
    }
    /**
     * Test of setCode method, of class TAirport.
     */
    @Test
    public void testSetCode() {
        System.out.println("setCode");
        String code = "Infernus";
        TAirport instance = new TAirport();
        instance.setCode(code);
        assertEquals(code, instance.getCode());
    }

    /**
     * Test of toString method, of class TAirport.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        TAirport instance = new TAirport();
        instance.setCity("test");
        instance.setCode("test2");
        instance.setCountry("Poland");
        String expResult =  "test" + ", " + "Poland" + " - " + "test2";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    
}
